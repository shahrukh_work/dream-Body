
import UIKit
import Alamofire
import ObjectMapper


class Connectivity {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

let APIClientDefaultTimeOut = 40.0

class APIClient: APIClientHandler {
    
    fileprivate var clientDateFormatter: DateFormatter
    var isConnectedToNetwork: Bool?
    
    static var shared: APIClient = {
        let baseURL = URL(fileURLWithPath: APIRoutes.baseUrl)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIClientDefaultTimeOut
        
        let instance = APIClient(baseURL: baseURL, configuration: configuration)
        
        return instance
    }()
    
    // MARK: - init methods
    
    override init(baseURL: URL, configuration: URLSessionConfiguration, delegate: SessionDelegate = SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        clientDateFormatter = DateFormatter()
        
        super.init(baseURL: baseURL, configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        
        //        clientDateFormatter.timeZone = NSTimeZone(name: "UTC")
        clientDateFormatter.dateFormat = "yyyy-MM-dd" // Change it to desired date format to be used in All Apis
    }
    
    
    // MARK: Helper methods
    
    func apiClientDateFormatter() -> DateFormatter {
        return clientDateFormatter.copy() as! DateFormatter
    }
    
    fileprivate func normalizeString(_ value: AnyObject?) -> String {
        return value == nil ? "" : value as! String
    }
    
    fileprivate func normalizeDate(_ date: Date?) -> String {
        return date == nil ? "" : clientDateFormatter.string(from: date!)
    }
    
    var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func getUrlFromParam(apiUrl: String, params: [String: AnyObject]) -> String {
        var url = apiUrl + "?"
        
        for (key, value) in params {
            url = url + key + "=" + "\(value)&"
        }
        url.removeLast()
        return url
    }
    
    // MARK: - Onboarding
    func updateToken(token: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["fcm": token] as [String:String]
        rawRequest(url: APIRoutes.tokenUpdate, method: .post, parameters: params as [String: AnyObject], headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"], completionBlock: completionBlock)
    }
    
    func signInMethod(email: String , password: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["username": email, "password": password] as [String:String]
        rawRequest(url: APIRoutes.login, method: .post, parameters: params as [String: AnyObject], headers: nil, completionBlock: completionBlock)
    }
    
    func createUser(params: [String: String],_ completionBlock: @escaping APIClientCompletionHandler) {
        
        rawRequest(url:APIRoutes.createUser, method: .post, parameters: params as [String: AnyObject], headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"],completionBlock: completionBlock)
    }
    
    func getChartHisrtory(_ year: String, _ month: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["year": year, "month": month] as [String:String]
        rawRequest(url:APIRoutes.history, method: .post, parameters: params as [String: AnyObject], headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"],completionBlock: completionBlock)
    }
    
    func verifyPhoneNumber(isVerified: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        
        _ = sendRequest(APIRoutes.verifyNumber , parameters: ["is_verified": isVerified] as [String : AnyObject],httpMethod: .post , headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"], completionBlock: completionBlock)
    }
    
    func userDetailMethod( _ completionBlock: @escaping APIClientCompletionHandler) {
        _ = sendRequest(APIRoutes.userDetail , parameters: nil ,httpMethod: .get , headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"], completionBlock: completionBlock)
    }
    
    
    func getVideos( _ completionBlock: @escaping APIClientCompletionHandler) {
        _ = sendRequest(APIRoutes.getVideos , parameters: nil ,httpMethod: .get , headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"], completionBlock: completionBlock)
    }
    
    
    
    func getUserDetaisListsMethod(id: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        
        
        let dateString = dateFormatter.string(from: currentDate)
        print(dateString)
        
        let params = ["id": id, "currentDate": dateString,"timezone":TimeZone.current.identifier.lowercased()] as [String:AnyObject]
        _ = sendRequest(APIRoutes.getDays , parameters: params ,httpMethod: .get , headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"], completionBlock: completionBlock)
    }
    
    func addDayMethod(day: String, key: String, value: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["post_title":day, "key":key, "value": value] as [String:AnyObject]
        _ = sendRequest(APIRoutes.addDay , parameters: params ,httpMethod: .post , headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"], completionBlock: completionBlock)
    }
    
    func updateValue(postId: String, key: String, value: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["post_id":postId, "key":key, "value": value, "timezone":TimeZone.current.identifier.lowercased()] as [String:AnyObject]
        _ = sendRequest(APIRoutes.updateValue , parameters: params ,httpMethod: .post , headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"], completionBlock: completionBlock)
    }
    
    func weightLossMethod(isFav: Bool?, _ completionBlock: @escaping APIClientCompletionHandler) {
        var params: [String: String] = [:]
        
        if let fav = isFav {
            
            if fav {
                params = ["is_fav":"true"]

            } else {
                params = ["is_fav":"false"]
            }
            
        } else {
        }
        
        _ = sendRequest(APIRoutes.weightLoss , parameters: params as! [String:AnyObject] ,httpMethod: .get , headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"], completionBlock: completionBlock)
    }
    
    
    func createUser2(params: [String: String], avs: String ,_ completionBlock: @escaping APIClientCompletionHandler) {
        _ = sendRequest(APIRoutes.createUser , parameters: params as [String : AnyObject],httpMethod: .post , headers: nil, completionBlock: completionBlock)
    }
    
    func forgotPasswordMethod(email: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["email_id": email, "base_url": "https://doro.codesorbit.com/home"]
        _ = sendRequest(APIRoutes.forgotPassword , parameters: params as [String : AnyObject],httpMethod: .post , headers: nil, completionBlock: completionBlock)
    }
    
    func favoriteMethod(postID: String, key: String, isFav: Bool, _ completionBlock: @escaping APIClientCompletionHandler) {
        
        var favorite = "false"
        
        if isFav {
            favorite = "true"
        }
        
        let params = ["post_id": postID, "key": "wpl_is_fav_", "value": favorite] as [String: AnyObject]
        _ = sendRequest(APIRoutes.favorite , parameters: params as [String : AnyObject],httpMethod: .post , headers: ["Authorization":"Bearer \(DataManager.shared.getAuthentication()?.token ?? "")"], completionBlock: completionBlock)
    }
    
    func updateUser(params: [String: String],_ completionBlock: @escaping APIClientCompletionHandler) {
        _ = sendRequest(APIRoutes.updateUser , parameters: params as [String : AnyObject],httpMethod: .put , headers: nil, completionBlock: completionBlock)
    }
    
    func updateUserProfile(image: UIImage, name: String, email: String, phoneNo: String, nationalId: String, ssn: String, occupation: String, type: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
//        let headers = ["Authorization": "Bearer "+DataManager.shared.getUser()!.token]
//        let params = ["image": image, "full_name": name, "email": email, "phone_number": phoneNo, "national_id": nationalId, "social_security_number": ssn, "occupation": occupation ,"type": type] as [String : Any]
//        sendRequestUsingMultipart(APIRoutes.baseUrl+APIRoutes.updateUser, parameters: params as [String : AnyObject] , httpMethod: .put, headers: headers, completionBlock: completionBlock)
    }
    
    func updateProfileData(firstName: String, lastName: String, dob: String,image: UIImage?, _ completion : @escaping (_ message: String?) -> () ) {
        
        let url = APIRoutes.baseUrl + APIRoutes.updateUser
        let tokenString = "Bearer " + "\(DataManager.shared.getAuthentication()?.token ?? "")"
        let header: HTTPHeaders = ["Accept": "accepts/json", "Content-Type":  "application/json", "Authorization":tokenString]
        
        var parameters = ["display_name": firstName, "user_nicename": lastName, "image": image, "DOB": dob] as [String : Any]
        
        Utility.showLoading()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters {
                
                if key == "image" {
                    let image = value as? UIImage
                    let data = image?.jpeg(.lowest)
                    if let newDate = data {
                        multipartFormData.append(data!, withName: "image", fileName: "UseerImage.jpeg", mimeType: "image/jpeg")
                    }
                    parameters.removeValue(forKey: "image")
                } else {
                    multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
                }
            }
            
        }, usingThreshold: 0, to: url, method: .post, headers: header, encodingCompletion: { (encodingResult) in
            
            switch encodingResult {
            case .success(let upload, _ , _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success(let json):
                        self.showRequestDetailForSuccess(responseObject: response)
                        
                        if let jsonObject = json as? [String:AnyObject] {
                            let errorCount = jsonObject["errors"] as? [String:Any]
                            
                            if errorCount == nil {
                                completion("User successfully update.")
                            } else {
                                
                            }
                        }
                        break
                        
                    case .failure(let error):
                        self.showRequestDetailForFailure(responseObject: response)
                        completion("User not failed to update.")
                        break
                    }
                }
            case .failure:
                break
            }
        })
    }
    
    
    func uploadVideo(title: String, description: String, videoURL: URL, _ completion : @escaping (_ message: String?) -> () ) {
        
        let url = APIRoutes.baseUrl + APIRoutes.addVideo
        let tokenString = "Bearer " + "\(DataManager.shared.getAuthentication()?.token ?? "")"
        let header: HTTPHeaders = ["Authorization":tokenString]
        
        var parameters = ["video_title": "This is for you ahsan", "video_description": "this is my first video", "video":videoURL] as [String : Any]
        
        Utility.showLoading()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            print(parameters)
            for (key, value) in parameters {
                
                if key == "video" {
                    do {
                           let videoData = try Data(contentsOf: videoURL)
                        multipartFormData.append(videoData, withName: "video", fileName: "app_video.mp4", mimeType: "video/mp4")
                       } catch {
                          // debugPrint("Couldn't get Data from URL: \(videoUrl): \(error)")
                       }
                } else {
                    multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
                }
            }
            
        }, usingThreshold: 0, to: url, method: .post, headers: header, encodingCompletion: { (encodingResult) in
            Utility.hideLoading()
            switch encodingResult {
            case .success(let upload, _ , _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success(let json):
                        self.showRequestDetailForSuccess(responseObject: response)
                        
                        if let jsonObject = json as? [String:AnyObject] {
                            let errorCount = jsonObject["errors"] as? [String:Any]
                            
                            if errorCount == nil {
                                completion("User successfully update.")
                            } else {
                                
                            }
                        }
                        break
                        
                    case .failure(let error):
                        self.showRequestDetailForFailure(responseObject: response)
                        print(response.request)
                        completion("User not failed to update.")
                        break
                    }
                }
            case .failure:
                break
            }
        })
    }
}

