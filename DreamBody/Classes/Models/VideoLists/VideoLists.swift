

import Foundation
import ObjectMapper

typealias VideoListCompletionHandler = (_ user: VideoLists?, _ status: Int?) -> ()


struct VideoLists : Mappable {
	var data : [VideoData]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		data <- map["data"]
	}

    static func loadData (_ completion: @escaping VideoListCompletionHandler) {
        
        APIClient.shared.getVideos { result, error, status in
            
            if error == nil {
                
                let json = ["data": result]
                if let data = Mapper<VideoLists>().map(JSON: json as [String : Any]) {
                    completion(data, status)
                    
                } else {
                    completion(nil, status)
                }
                
            } else {
                completion(nil, status)
            }
        }
    }
}
