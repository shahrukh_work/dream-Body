

import Foundation
import ObjectMapper

struct VideoData : Mappable {
	var post_excerpt : String?
	var post_date : String?
	var post_id : String?
	var video_link : String?
	var post_title : String?
	var video_user_id : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		post_excerpt <- map["post_excerpt"]
		post_date <- map["post_date"]
		post_id <- map["post_id"]
		video_link <- map["video_link"]
		post_title <- map["post_title"]
		video_user_id <- map["video_user_id"]
	}

}
