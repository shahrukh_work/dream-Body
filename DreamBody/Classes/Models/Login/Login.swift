//
//  Login.swift
//  Dream-Body
//
//  Created by MAC on 18/08/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import Foundation
import ObjectMapper

typealias LoginCompletionHandler = (_ user: Login?, _ status: Int?) -> ()
typealias LoginErrorCompletionHandler = (_ error: Error?,_ status: Int?,_ message: String) -> ()


class Login: Mappable {
    static var shared = Login()
    var token = ""
    var userEmail = ""
    var userName = ""
    var displayName = ""
    
    init () {
        
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        token <- map["token"]
        userEmail <- map["user_email"]
        userName <- map["user_nicename"]
        displayName <- map["user_display_name"]
    }
    
    class func login(email: String, password: String, _ completions: @escaping LoginCompletionHandler, _ errorCompletion: @escaping LoginErrorCompletionHandler) {
        
        APIClient.shared.signInMethod(email: email, password: password) { (result, error, status) in
            
            if error == nil {
                
                if let data = result?["data"] as? [String: Any] {
                    
                    if let newStatus  = data["status"] as? Int {
                        
                        if newStatus == 403 {
                            errorCompletion(error,newStatus, (result?["message"] as? String) ?? "")
                             return
                        }
                    }
                }
                

                
                if let data = Mapper<Login>().map(JSON: result as! [String : Any]) {
                    Login.shared = data
                    DataManager.shared.setAuthentication(user: data.toJSONString() ?? "")
                    completions(data, status)
                    
                } else {
                    errorCompletion(error,status, "")
                }
            }
        }
    }
}
