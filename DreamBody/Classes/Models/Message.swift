//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Asad Mehmood on 06/09/2021.
//  Copyright © 2021 Angela Yu. All rights reserved.
//

import Foundation
import FirebaseFirestore

struct Message {
    let dateTime: Date
    let message: String
    let uid: String

    
    
    init(dictionary: [String: Any]) {
        self.dateTime =  (dictionary["dateTime"] as! Timestamp).dateValue()
        self.message = dictionary["message"] as? String ?? ""
        self.uid = dictionary["uid"] as? String ?? ""
        
        
    }
}

