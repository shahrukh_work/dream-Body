//
//  Chat.swift
//  Flash Chat iOS13
//
//  Created by Asad Mehmood on 13/09/2021.
//  Copyright © 2021 Angela Yu. All rights reserved.
//

import Foundation

struct Chat{
    
    let date: Date
    let lastMessage: String
    let name: String
    let uid : String
    let userImage: String

    init(dictionary: [String: Any]) {
        self.date = dictionary["date"] as? Date ?? Date()
        self.lastMessage = dictionary["latestMessage"] as? String ?? ""
        self.name = dictionary["name"] as? String ?? ""
        self.uid = dictionary["uid"] as? String ?? ""
        self.userImage = dictionary["userImage"] as? String ?? ""
    }
}
