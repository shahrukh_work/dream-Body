

import Foundation
import ObjectMapper

typealias CreateUserCompletionHandler = (_ user: User?, _ status: Int?) -> ()
typealias CreateUserErrorCompletionHandler = (_ error: Error?,_ status: Int?) -> ()
typealias UserDetailErrorCompletionHandler = (_ error: Error?,_ status: Int?) -> ()
typealias UserDetailCompletionHandler = (_ user: UserDetail?, _ status: Int?) -> ()



class User : Mappable {

	var id : Int?
	var description : String?
	var roles : [String]?
	var registered_date : String?
	var avatar_urls : AvatarUrls?
	var link : String?
	var url : String?
	var locale : String?
	var capabilities : Capabilities?
	var extra_capabilities : ExtraCapabilities?
	var first_name : String?
	var slug : String?
	var username : String?
	var meta : [String]?
	var last_name : String?
	var email : String?
	var name : String?
	var nickname : String?
    var profileImage: String?

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		id <- map["id"]
		description <- map["description"]
		roles <- map["roles"]
		registered_date <- map["registered_date"]
		avatar_urls <- map["avatar_urls"]
		link <- map["link"]
		url <- map["url"]
		locale <- map["locale"]
		capabilities <- map["capabilities"]
		extra_capabilities <- map["extra_capabilities"]
		first_name <- map["first_name"]
		slug <- map["slug"]
		username <- map["username"]
		meta <- map["meta"]
		last_name <- map["last_name"]
		email <- map["email"]
		name <- map["name"]
		nickname <- map["nickname"]
	}

    class func signUp(userName: String, name: String, email: String, password: String, _ completions: @escaping CreateUserCompletionHandler, _ errorCompletion: @escaping CreateUserErrorCompletionHandler) {
        Utility.showLoading()
        let json = ["username": userName,"name": name, "email": email, "password": password]
        
        APIClient.shared.createUser(params: json) { (result, error, status) in
            if error == nil {
                
                if let data = Mapper<User>().map(JSON: result as! [String : Any]) {
                    DataManager.shared.setUser(user: data.toJSONString() ?? "")
                    completions(data, status)

                } else {
                    errorCompletion(error,status)
                }
            }
        }
    }
}


class UserDetail: Mappable {
    
    static var shared = UserDetail()
    var id = ""
    var userLogin = ""
    var userPass = ""
    var name = ""
    var email = ""
    var activationKey = ""
    var status = ""
    var displayName = ""
    var image = ""
    var dob = ""
    var phoneNumberVerification = ""
    var userType = ""

    init() {
        
    }
    
    required init?(map: Map) {

    }

    func mapping(map: Map) {

        id  <- map["ID"]
        userLogin  <- map["user_login"]
        userPass  <- map["user_pass"]
        name  <- map["user_nicename"]
        email  <- map["user_email"]
        activationKey  <- map["user_activation_key"]
        status  <- map["user_status"]
        displayName  <- map["display_name"]
        image  <- map["profile_image"]
        dob <- map["DOB"]
        phoneNumberVerification <- map["phone_number_verification"]
        userType <- map["userType"]
    }

    class func userDetail( _ completions: @escaping UserDetailCompletionHandler, _ errorCompletion: @escaping UserDetailErrorCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.userDetailMethod { (result, error, status) in
            Utility.hideLoading()
            if error == nil {
                
                if let data = Mapper<UserDetail>().map(JSON: result as! [String : Any]) {
                    DataManager.shared.setUserDetail(user:  data.toJSONString() ?? "")
                    UserDetail.shared = data
                    completions(data, status)

                } else {
                    errorCompletion(error,status)
                }
                
            } else {
                errorCompletion(error,status)
            }
        }
    }
    
    class func verifyPhoneNumber(isVerified: Int, _ completions: @escaping UserDetailCompletionHandler, _ errorCompletion: @escaping UserDetailErrorCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.verifyPhoneNumber(isVerified: isVerified) { (result, error, status) in
            Utility.hideLoading()
            if error == nil {
                
                if let data = Mapper<UserDetail>().map(JSON: result as! [String : Any]) {
                   // DataManager.shared.setUser(user:  data.toJSONString() ?? "")
                   // UserDetail.shared = data
                    completions(data, status)

                } else {
                    errorCompletion(error,status)
                }
            }
        }
    }
}
