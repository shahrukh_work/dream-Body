//
//  GetDaysList.swift
//  Dream-Body
//
//  Created by MAC on 19/08/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import Foundation
import ObjectMapper

typealias GetDaysListCompletionHandler = (_ user: [GetDays]?, _ status: Int?) -> ()

typealias GetDaysCompletionHandler = (_ user: GetDays?, _ status: Int?) -> ()
typealias GetDaysErrorCompletionHandler = (_ error: Error?, _ items: Int, _ status: Int?) -> ()


class GetDays: Mappable {
    static var shared = GetDays()
    var postId = ""
    var title = ""
    var content = ""
    var weightOffice = ""
    var weightHome = ""
    var hunger = ""
    var bowelMovement = ""
    var prevacID = ""
    var urineKeytones = ""
    var f = ""
    var laxative = ""
    var user = ""
    var gastricIssues = ""
    
    init () {
        
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        postId <- map["post_id"]
        title <- map["post_title"]
        content <- map["post_content"]
        weightOffice <- map["wpl_weight_office"]
        weightHome <- map["wpl_weight_home"]
        hunger <- map["wpl_hunger"]
        bowelMovement <- map["wpl_bowel_movement"]
        prevacID <- map["wpl_prevacid"]
        urineKeytones <- map["wpl_urine_ketones"]
        f <- map["wpl_f"]
        laxative <- map["wpl_laxative"]
        user <- map["wpl_user"]
    }
    
    class func getDaysList(id: String = DataManager.shared.getUserDetail()?.id ?? "", _ completions: @escaping GetDaysListCompletionHandler, _ errorCompletion: @escaping GetDaysErrorCompletionHandler) {
        
        APIClient.shared.getUserDetaisListsMethod(id: id) { (result, error, status) in
            
            if error == nil {
                
                if (result?.count ?? 0) == 0 {
                    errorCompletion(error, result?.count ?? 0, status)
                    return
                }
                
                if result != nil {
                    let data = Mapper<GetDays>().mapArray(JSONArray: (result as? [[String : Any]]) ?? [])
                    completions(data, status)
                    
                } else {
                    errorCompletion(error, result?.count ?? 0, status)
                }
            }
        }
    }
    
    class func addDay(day: String = "Day 1", key: String = "wpl_weight_office", value: String = "0lbs", _ completions: @escaping GetDaysCompletionHandler, _ errorCompletion: @escaping GetDaysErrorCompletionHandler) {
        
        APIClient.shared.addDayMethod(day: day, key: key, value: value) { (result, error, status) in
            
            if error == nil {
                
                if (result?.count ?? 0) == 0 {
                    errorCompletion(error, result?.count ?? 0, status)
                    return
                }
                
                if let data = Mapper<GetDays>().map(JSON: result as! [String : Any]) {
                    completions(data, status)
                    
                } else {
                    errorCompletion(error, result?.count ?? 0, status)
                }
            }
        }
    }
}
