





import Foundation
import ObjectMapper

typealias WeightInfoCompletionHandler = (_ user: [WeightInfo]?, _ status: Int?) -> ()
typealias WeightInfoErrorCompletionHandler = (_ error: Error?,_ status: Int?) -> ()

class WeightInfo : Mappable {
	var iD : String?
	var post_author : String?
	var post_date : String?
	var post_date_gmt : String?
	var post_content : String?
	var post_title : String?
	var post_excerpt : String?
	var post_status : String?
	var comment_status : String?
	var ping_status : String?
	var post_password : String?
	var post_name : String?
	var to_ping : String?
	var pinged : String?
	var post_modified : String?
	var post_modified_gmt : String?
	var post_content_filtered : String?
	var post_parent : String?
	var guid : String?
	var menu_order : String?
	var post_type : String?
	var post_mime_type : String?
	var comment_count : String?
	var meta_id : String?
	var post_id : String?
	var meta_key : String?
	var meta_value : String?
    var isFav = false
    var wpl_is_fav : String? {
        
        didSet {
            if wpl_is_fav == "true" {
                isFav = true
            } else {
                isFav = false
            }
        }
    }
    
    var thumbNail = ""

	required init?(map: Map) {}

    func mapping(map: Map) {

		iD <- map["ID"]
		post_author <- map["post_author"]
		post_date <- map["post_date"]
		post_date_gmt <- map["post_date_gmt"]
		post_content <- map["post_content"]
		post_title <- map["post_title"]
		post_excerpt <- map["post_excerpt"]
		post_status <- map["post_status"]
		comment_status <- map["comment_status"]
		ping_status <- map["ping_status"]
		post_password <- map["post_password"]
		post_name <- map["post_name"]
		to_ping <- map["to_ping"]
		pinged <- map["pinged"]
		post_modified <- map["post_modified"]
		post_modified_gmt <- map["post_modified_gmt"]
		post_content_filtered <- map["post_content_filtered"]
		post_parent <- map["post_parent"]
		guid <- map["guid"]
		menu_order <- map["menu_order"]
		post_type <- map["post_type"]
		post_mime_type <- map["post_mime_type"]
		comment_count <- map["comment_count"]
		meta_id <- map["meta_id"]
		post_id <- map["post_id"]
		meta_key <- map["meta_key"]
		meta_value <- map["meta_value"]
		wpl_is_fav <- map["wpl_is_fav"]
        thumbNail <- map["thumbnail"]
	}
    

    class func getWeightInfoList(isFav: Bool?, _ completions: @escaping WeightInfoCompletionHandler, _ errorCompletion: @escaping WeightInfoErrorCompletionHandler) {
        
       // Utility.showLoading()
        APIClient.shared.weightLossMethod(isFav: isFav) { (result, error, status) in
        //    Utility.hideLoading()
            
            if error == nil {
                
                let data = Mapper<WeightInfo>().mapArray(JSONArray: result as! [[String: Any]])
                
                if data != nil {
                    completions(data, status)
                    
                } else  {
                    errorCompletion(error,status)
                }
                
            } else {
                errorCompletion(error,status)
            }
        }
    }
}
