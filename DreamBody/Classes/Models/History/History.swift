





import Foundation
import ObjectMapper

struct History : Mappable {
	var post_id : String?
	var post_title : String?
	var post_content : String?
	var post_date : String?
	var wpl_weight_office : String?
	var wpl_weight_home : String?
	var wpl_hunger : String?
	var wpl_bowel_movement : String?
	var wpl_prevacid : String?
	var wpl_urine_ketones : String?
	var wpl_f : String?
	var wpl_laxative : String?
	var wpl_user : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		post_id <- map["post_id"]
		post_title <- map["post_title"]
		post_content <- map["post_content"]
		post_date <- map["post_date"]
		wpl_weight_office <- map["wpl_weight_office"]
		wpl_weight_home <- map["wpl_weight_home"]
		wpl_hunger <- map["wpl_hunger"]
		wpl_bowel_movement <- map["wpl_bowel_movement"]
		wpl_prevacid <- map["wpl_prevacid"]
		wpl_urine_ketones <- map["wpl_urine_ketones"]
		wpl_f <- map["wpl_f"]
		wpl_laxative <- map["wpl_laxative"]
		wpl_user <- map["wpl_user"]
	}

}
