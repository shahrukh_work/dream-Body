




import Foundation
import ObjectMapper

typealias ChartHistoryCompletionHandler = (_ user: [History]?, _ status: Int?) -> ()


struct ChartHistory : Mappable {
	var data : [History]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		data <- map["data"]
	}

    static func getHistory(year: String, month: String, _ completions: @escaping ChartHistoryCompletionHandler, _ errorCompletion: @escaping CreateUserErrorCompletionHandler) {
        
        APIClient.shared.getChartHisrtory(year, month) { (result, error, status) in

            if error == nil {
                
                let data = Mapper<ChartHistory>().map(JSON: result as! [String : Any])
                
                if data != nil {
                    completions(data?.data, status)
                    
                } else  {
                    errorCompletion(error,status)
                }
                
            } else {
                errorCompletion(error,status)
            }
        }
    }
}
