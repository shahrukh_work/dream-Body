//
//  HomeViewController.swift
//  Dream-Body
//
//  Created by MAC on 26/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit
import SkeletonView
import NVActivityIndicatorView

class HomeViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var pleaseWaitLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var topViews: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var lostWeightLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    
    var getDays: [GetDays] = []
    var isLoading = false
    
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tokenUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
        self.noDataLabel.isHidden = true
    }
    
    //MARK: - Setup
    func setup () {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        loadData()
    }
    
    func loadData () {
        isLoading = true
        
        let user = DataManager.shared.getUserDetail()
        firstNameLabel.text = user?.name.components(separatedBy: " ").last
        lastNameLabel.text = user?.displayName.components(separatedBy: " ").first
        dateLabel.text = DataManager.shared.getUser()?.registered_date
        dobLabel.text = DataManager.shared.getUser()?.registered_date
        lostWeightLabel.text = "25lbs"
        
        self.pleaseWaitLabel.isHidden = true
        if DataManager.shared.getFirstTimSignUp() {
            self.pleaseWaitLabel.isHidden = false
            UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                self.pleaseWaitLabel.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            }, completion: nil)
        }
        
        showHideSkeleton(show: true)
        
        GetDays.getDaysList { (result, status) in
            self.isLoading = false
            self.getDays = result ?? []
            self.pleaseWaitLabel.isHidden = true
            DataManager.shared.setFirstTimSignUp(enable: false)
            if self.getDays.count == 0 {
                self.noDataLabel.isHidden = false
            }
            self.tableView.reloadData()
            self.showHideSkeleton(show: false)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                Utility.takeScreenShot(view: self.view)
            }
            
        } _: { (error, count, status)  in
            self.showAlert(title: "", message: error?.localizedDescription ?? "")
        }

    }
    
    func showHideSkeleton (show: Bool) {
        firstNameLabel.isSkeletonable = true
        lastNameLabel.isSkeletonable = true
        dateLabel.isSkeletonable = true
        dobLabel.isSkeletonable = true
        lostWeightLabel.isSkeletonable = true
        headingLabel.isSkeletonable = true
        topViews.isSkeletonable = true
        
        if show {
            firstNameLabel.showGradientSkeleton()
            lastNameLabel.showGradientSkeleton()
            dateLabel.showGradientSkeleton()
            dobLabel.showGradientSkeleton()
            lostWeightLabel.showGradientSkeleton()
            self.headingLabel.showGradientSkeleton()
            topViews.showGradientSkeleton()

        } else {
            self.headingLabel.hideSkeleton()
            self.topViews.hideSkeleton()
            firstNameLabel.hideSkeleton()
            lastNameLabel.hideSkeleton()
            dateLabel.hideSkeleton()
            dobLabel.hideSkeleton()
            lostWeightLabel.hideSkeleton()
        }
    }
    
    @IBAction func openSideMenuPressed(_ sender: Any) {
        self.openLeft()
    }
    
    func setValues (postId: String, key: String, value: String) {

        Utility.showLoading()
        APIClient.shared.updateValue(postId: postId, key: key, value: value) { (result, error, status) in
            Utility.hideLoading()
        print(result)
            if error == nil {
                if let newResult = result as? [String: Any] {
                    
                    if let success = newResult["success"] as? Bool {
                        
                        if !success {
                            self.showAlert(title: "Error", message:  (newResult["message"] as? String) ?? "")
                            
                            return
                        }
                    }
                }
                self.dismiss(animated: true, completion: nil)
                
            } else {
                self.showAlert(title: "error", message: error?.localizedDescription ?? "")
            }
        }
    }
    
    func tokenUpdate () {
        APIClient.shared.updateToken(token: Utility.getAppDelegate()!.fcmToken) { result, error, status in
            
            if error == nil {
                print("token updated!")
            }
        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDatasource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  self.getDays.count == 0 && isLoading {
            return 4
        }
        return self.getDays.count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(LBSTableViewCell.self, indexPath: indexPath)
        
        if getDays.count == 0 && isLoading {
            cell.showHideSkeleton(show: true)
            
        } else {
            cell.showHideSkeleton(show: false)
            cell.configure(data: getDays[indexPath.row])
            cell.pickerCallBack = { (postId, key, value) in
                self.setValues(postId: postId, key: key, value: value)
            }
            cell.callBack = { (postId, key) in
                let vc = UpdateViewController()
                vc.modalPresentationStyle = .overCurrentContext
                vc.postId = "\(postId)"
                vc.key = key
                vc.callBack = {
                    self.loadData()
                }
                self.present(vc, animated: true, completion: nil)
            }

        }
        cell.frame = tableView.bounds
        return cell
    }
}
