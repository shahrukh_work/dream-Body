//
//  LBSTableViewCell.swift
//  Dream-Body
//
//  Created by MAC on 27/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit
import SkeletonView

class CustomCollectionView: UICollectionView {
    override func layoutSubviews() {
            super.layoutSubviews()
            if !(__CGSizeEqualToSize(bounds.size,self.intrinsicContentSize)){
                self.invalidateIntrinsicContentSize()
            }
        }
        override var intrinsicContentSize: CGSize {
            return contentSize
        }
}

enum SelectedPicker {
    case hunger
    case gastric
}

class LBSTableViewCell: UITableViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var allLabelsView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var wightOfficeLabel: UILabel!
    @IBOutlet weak var wightHomeLabel: UILabel!
    @IBOutlet weak var urineKetoneLabel: UILabel!
    @IBOutlet weak var hungerLabel: UILabel!
    @IBOutlet weak var bowelLabel: UILabel!
    @IBOutlet weak var prevacidLabel: UILabel!
    @IBOutlet weak var fLabel: UILabel!
    @IBOutlet weak var laxativeLabel: UILabel!
    @IBOutlet weak var hungerTextField: UITextField!
    @IBOutlet weak var gastricTextField: UITextField!
    
    
    @IBOutlet weak var gasticHeadingLabel: UILabel!
    @IBOutlet weak var gastricValueLabel: UILabel!
    @IBOutlet weak var weightOfficeHeadingLabel: UILabel!
    @IBOutlet weak var wightHomeHeadingLabel: UILabel!
    @IBOutlet weak var urineKeytoneHeadingLabel: UILabel!
    @IBOutlet weak var hungerHeadingLabel: UILabel!
    @IBOutlet weak var bowelHeadingLabel: UILabel!
    @IBOutlet weak var prevacidHeadingLabel: UILabel!
    @IBOutlet weak var fHeadingLabel: UILabel!
    @IBOutlet weak var laxativeHeadingLabel: UILabel!
    let pickerView: UIPickerView! = UIPickerView()

    
    
    var getDay: GetDays?
    
    var items = ["No Hunger", "A Little Hunger", "Some Hunger", "Hunger a lot"]
    var myItems: [String] = []
    var gastricItems = ["Constipation", "Diarrhea", "Queasy"]
    var callBack: ((_ postId: String, _ key: String) -> ())?
    var pickerCallBack: ((_ postId: String, _ key: String, _ value: String) -> ())?
    var pickers: SelectedPicker = .hunger
    var toolbarHunger: UIToolbar?
    var toolbarGastric: UIToolbar?
    
    var doneButtonHunger: UIBarButtonItem?
    var cancelButtonHunger: UIBarButtonItem?
    var spaceButtonHunger: UIBarButtonItem?
    
    var doneButtonGastric: UIBarButtonItem?
    var cancelButtonGastric: UIBarButtonItem?
    var spaceButtonGastric: UIBarButtonItem?
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        pickerView.dataSource = self
        pickerView.delegate = self
        hungerTextField.inputView = pickerView
        gastricTextField.inputView = pickerView
        setHungerToolBar()
        setGastricToolBar()
    }
    
    
    func configure (data: GetDays) {
        getDay = data
        
        print(data.postId ?? "")
        gastricValueLabel.text = data.laxative == "" ? "No Issues" : data.laxative
        dayLabel.text = data.title
        wightOfficeLabel.text = data.weightOffice == "" ? "-" : data.weightOffice
        wightHomeLabel.text = data.weightHome == "" ? "0lbs" : data.weightHome
        urineKetoneLabel.text = data.urineKeytones == "" ? "0lbs" : data.urineKeytones
        hungerLabel.text = data.hunger == "" ? "0lbs" : data.hunger
        bowelLabel.text = data.bowelMovement == "" ? "0lbs" : data.bowelMovement
       // prevacidLabel.text = data.prevacID == "" ? "0lbs" : data.prevacID
        fLabel.text = data.f == "" ? "0lbs" : data.f
        laxativeLabel.text = data.laxative == "" ? "0lbs" : data.laxative
    }
    
    func configure (data: History) {
        dayLabel.text = data.post_title
        wightOfficeLabel.text = data.wpl_weight_office == "" ? "-" : data.wpl_weight_office
        wightHomeLabel.text = data.wpl_weight_home == "" ? "0lbs" : data.wpl_weight_home
        urineKetoneLabel.text = data.wpl_urine_ketones == "" ? "0lbs" : data.wpl_urine_ketones
        hungerLabel.text = data.wpl_hunger == "" ? "0lbs" : data.wpl_hunger
        bowelLabel.text = data.wpl_bowel_movement == "" ? "0lbs" : data.wpl_bowel_movement
      //  prevacidLabel.text = data.wpl_prevacid == "" ? "0lbs" : data.wpl_prevacid
        fLabel.text = data.wpl_f == "" ? "0lbs" : data.wpl_f
        laxativeLabel.text = data.wpl_laxative == "" ? "0lbs" : data.wpl_laxative
    }
    
    
    func showHideSkeleton (show: Bool) {
        allLabelsView.isSkeletonable = false
        dayLabel.isSkeletonable = true
        wightOfficeLabel.isSkeletonable = true
        wightHomeLabel.isSkeletonable = true
        urineKetoneLabel.isSkeletonable = true
        hungerLabel.isSkeletonable = true
        bowelLabel.isSkeletonable = true
        gastricValueLabel.isSkeletonable = true
        gasticHeadingLabel.isSkeletonable = true
       // prevacidLabel.isSkeletonable = true
        fLabel.isSkeletonable = true
        laxativeLabel.isSkeletonable = true
        
        weightOfficeHeadingLabel.isSkeletonable = true
        wightHomeHeadingLabel.isSkeletonable = true
        bowelHeadingLabel.isSkeletonable = true
        urineKeytoneHeadingLabel.isSkeletonable = true
        hungerHeadingLabel.isSkeletonable = true
       // prevacidHeadingLabel.isSkeletonable = true
        fHeadingLabel.isSkeletonable = true
        laxativeHeadingLabel.isSkeletonable = true
        
        
        if show {
            gastricValueLabel.showGradientSkeleton()
            gasticHeadingLabel.showGradientSkeleton()
            allLabelsView.showGradientSkeleton()
            dayLabel.showGradientSkeleton()
            wightOfficeLabel.showGradientSkeleton()
            wightHomeLabel.showGradientSkeleton()
        
            urineKetoneLabel.showGradientSkeleton()
            hungerLabel.showGradientSkeleton()
            bowelLabel.showGradientSkeleton()
         //   prevacidLabel.showGradientSkeleton()
            fLabel.showGradientSkeleton()
            laxativeLabel.showGradientSkeleton()
            
            weightOfficeHeadingLabel.showGradientSkeleton()
            wightHomeHeadingLabel.showGradientSkeleton()
            bowelHeadingLabel.showGradientSkeleton()
            urineKeytoneHeadingLabel.showGradientSkeleton()
            hungerHeadingLabel.showGradientSkeleton()
          //  prevacidHeadingLabel.showGradientSkeleton()
            fHeadingLabel.showGradientSkeleton()
            laxativeHeadingLabel.showGradientSkeleton()

        } else {
            gastricValueLabel.hideSkeleton()
            gasticHeadingLabel.hideSkeleton()
            allLabelsView.hideSkeleton()
            dayLabel.hideSkeleton()
            wightOfficeLabel.hideSkeleton()
            wightHomeLabel.hideSkeleton()
            
            urineKetoneLabel.hideSkeleton()
            hungerLabel.hideSkeleton()
            bowelLabel.hideSkeleton()
          //  prevacidLabel.hideSkeleton()
            fLabel.hideSkeleton()
            laxativeLabel.hideSkeleton()
            
            weightOfficeHeadingLabel.hideSkeleton()
            wightHomeHeadingLabel.hideSkeleton()
            bowelHeadingLabel.hideSkeleton()
            urineKeytoneHeadingLabel.hideSkeleton()
            hungerHeadingLabel.hideSkeleton()
          //  prevacidHeadingLabel.hideSkeleton()
            fHeadingLabel.hideSkeleton()
            laxativeHeadingLabel.hideSkeleton()
        }
    }
    
    
    //MARK: - Actions
    @IBAction func wightOfficePressed(_ sender: Any) {
        callBack?(getDay?.postId ?? "", "wpl_weight_office")
    }
    
    @IBAction func weightHomePressed(_ sender: Any) {
        callBack?(getDay?.postId ?? "", "wpl_weight_home")
    }
    
    @IBAction func ketonePressed(_ sender: Any) {
        callBack?(getDay?.postId ?? "", "wpl_urine_ketones")
    }
    
    @IBAction func hungerPressed(_ sender: Any) {
        //callBack?(getDay?.postId ?? "", "wpl_hunger")
        pickers = .hunger
        myItems = items
        pickerView.reloadAllComponents()
        hungerTextField.becomeFirstResponder()
    }
    
    @IBAction func bowelPressed(_ sender: Any) {
        callBack?(getDay?.postId ?? "", "wpl_bowel_movement")
    }
    
    @IBAction func prevacyPressed(_ sender: Any) {
        callBack?(getDay?.postId ?? "", "wpl_prevacid")
    }
    
    @IBAction func fPressed(_ sender: Any) {
        callBack?(getDay?.postId ?? "", "wpl_f")
    }
    
    @IBAction func laxativePressed(_ sender: Any) {
    }
    
    @IBAction func gastricIssuePressed(_ sender: Any) {
        pickers = .gastric
        myItems = gastricItems
        pickerView.reloadAllComponents()
        gastricTextField.becomeFirstResponder()
    }
    
    func setHungerToolBar () {
        toolbarHunger = UIToolbar()
        toolbarHunger!.sizeToFit()
        doneButtonHunger = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePickerHunger))
        spaceButtonHunger = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        cancelButtonHunger = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPickerHunger))
        toolbarHunger!.setItems([doneButtonHunger!,spaceButtonHunger!,cancelButtonHunger!], animated: false)
        hungerTextField.inputAccessoryView = toolbarHunger
    }
    
    func setGastricToolBar () {
        toolbarGastric = UIToolbar()
        toolbarGastric!.sizeToFit()
        doneButtonGastric = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePickerGastric))
        spaceButtonGastric = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        cancelButtonGastric = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPickerGastric))
        toolbarGastric!.setItems([doneButtonGastric!,spaceButtonGastric!,cancelButtonGastric!], animated: false)
        gastricTextField.inputAccessoryView = toolbarGastric
    }
    
    @objc func donePickerHunger () {
        pickerCallBack?(getDay?.postId ?? "", "wpl_hunger", getDay?.hunger ?? "")
        hungerTextField.resignFirstResponder()
    }
    
    @objc func cancelPickerHunger () {
        hungerTextField.resignFirstResponder()
    }
    
    @objc func donePickerGastric () {
        pickerCallBack?(getDay?.postId ?? "", "wpl_laxative", getDay?.laxative ?? "")
        gastricTextField.resignFirstResponder()
    }
    
    @objc func cancelPickerGastric () {
        gastricTextField.resignFirstResponder()
    }
}


//MARK: - UIPickerViewDelegate & UIPickerViewDataSource
extension LBSTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return myItems.count
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return myItems[row]
    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickers == .hunger {
            hungerLabel.text = myItems[row]
            getDay?.hunger = myItems[row]
            
        } else {
            getDay?.laxative = myItems[row]
            gastricValueLabel.text = myItems[row]
        }
    }
}



//MARK: - UICollectionViewDelegate & UICollectionViewDatasource
