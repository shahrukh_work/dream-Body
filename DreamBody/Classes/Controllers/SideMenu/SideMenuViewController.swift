//
//  SideMenuViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/27/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
public var screenImage = UIImage()
class SideMenu {
    var title = ""
    var image = UIImage()
    var isSelected = false
}


class SideMenuViewController: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var sideViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var screenImageView: UIImageView!
    @IBOutlet weak var stackViewTransparent: UIView!
    let profileController = ProfileViewController()
    let homecontroller = HomeViewController()
    let weightLosscontroller = WeightLossViewController()
    let favcontroller = WeightLossViewController()
    let videoController = VideoLibraryViewController()
    let historyController = DayHistoryViewController()
    let messagingVc = MessagingViewController()
    
    
    //MARK: - Variables
    var status = ""
    let pickerView = UIPickerView()
    var statusList = ["Online", "Offline"]
    let selectedOptionIcons: [UIImage] = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "profile"), #imageLiteral(resourceName: "info"),#imageLiteral(resourceName: "info"),#imageLiteral(resourceName: "info"),#imageLiteral(resourceName: "info"),#imageLiteral(resourceName: "admin"), #imageLiteral(resourceName: "logout")]
    let optionIcons: [UIImage] = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "profile"), #imageLiteral(resourceName: "info"),#imageLiteral(resourceName: "info"),#imageLiteral(resourceName: "info"),#imageLiteral(resourceName: "info"),#imageLiteral(resourceName: "admin"), #imageLiteral(resourceName: "logout")]
    let options = ["Home", "Profile", "Favorite", "History","Weight Loss Info","Videos","Contact Admin","Logout"]
    var data: [SideMenu] = []
    var ration = 0.0
    
    
    //MARK: - ViewController  Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = #colorLiteral(red: 0.8431372549, green: 0.2, blue: 0.5058823529, alpha: 1)
        let user = DataManager.shared.getUserDetail()
        self.userNameLabel.text = "\(user?.name ?? "") \(user?.displayName ?? "")"
        profileImageView.sd_setImage(with: URL(string: DataManager.shared.getUserDetail()?.image ?? ""), placeholderImage: #imageLiteral(resourceName: "profile_image"))
        screenImageView.image = screenImage
    }
    
    //MARK: - View Setup
    private func setupView() {
        pickerView.delegate = self
        pickerView.dataSource = self
        
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner,.layerMinXMaxYCorner], view: stackViewTransparent, cornerRadius: 15)
        Utility.cornerRadiusPostioned(corners: [.layerMinXMinYCorner,.layerMinXMaxYCorner], view: shadowView, cornerRadius: 30)
        tableView.delegate = self
        tableView.dataSource = self
        dataSource ()
    }
    
    
    //MARK: - Datasource
    func dataSource () {
        
        for i in 0..<options.count {
            let obj = SideMenu()
            obj.title = options[i]
            obj.image = optionIcons[i]
            data.append(obj)
        }
    }
    
    
    //MARK: - Actions
    @IBAction func logoutButtonTapped(_ sender: Any) {
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        slideMenuController()?.closeLeft()
    }
    
    
    //MARK: - Private Methods
    private func pushController(controller: UIViewController) {
        
        let rootController = self.navigationController?.viewControllers.first(where: { (viewController) -> Bool in
            return viewController == controller
        })
        
        if let cntrlr = rootController {
            self.navigationController?.popToViewController(cntrlr, animated: false)
            
        } else {
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
}


extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(SideMenuTableViewCell.self, indexPath: indexPath)
        cell.selectionStyle = .none
        cell.configCell(item: data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 0: // home
            self.slideMenuController()?.changeMainViewController(homecontroller, close: true)
            slideMenuController()?.closeLeft()
            break
            
        case 1: // History
            self.slideMenuController()?.changeMainViewController(profileController, close: true)
           // slideMenuController()?.closeLeft()
            break
//
        case 2:
            favcontroller.isFavorite = true
            self.slideMenuController()?.changeMainViewController(favcontroller, close: true)
            //slideMenuController()?.closeLeft()
            break
            
        case 3:
            
            self.slideMenuController()?.changeMainViewController(historyController, close: true)
            //slideMenuController()?.closeLeft()
            break
            
        case 4:
            
            self.slideMenuController()?.changeMainViewController(weightLosscontroller, close: true)
            break
            
        case 5:
            
            self.slideMenuController()?.changeMainViewController(videoController, close: true)
            //slideMenuController()?.closeLeft()
            break
            
        case 6:
            
            messagingVc.userId = DataManager.shared.getUserDetail()?.id ?? ""
            self.slideMenuController()?.changeMainViewController(messagingVc, close: true)
            //slideMenuController()?.closeLeft()
            break
            
        case 7:
            DataManager.shared.deleteAuthentication()
            DataManager.shared.deleteUserDetail()
            Utility.loginRootViewController()
            
        default:
            break
        }
    }
}


//MARK: - SideMenuDelegate
extension SideMenuViewController : SlideMenuControllerDelegate {
    
    func screenRatio(ratio: CGFloat) {
        ration = Double(ratio)
        self.shadowView.alpha = ratio
        self.sideViewTopConstraint.constant = (ratio/0.0087)
        
        if ratio < 0.56 {
            self.stackViewTransparent.alpha = ratio
        }
        
        if self.sideViewWidthConstraint.constant < 90-(100-(ratio/0.01)) {
            print("width is ratio \(90-(100 - (ratio/0.01)))\n")
            self.sideViewWidthConstraint.constant = 90-(100 - (ratio/0.01))
        }
        
        self.sideViewBottomConstraint.constant = (ratio/0.0087)
    }
    
    func leftWillOpen() {
        UIView.animate(withDuration: 0.5, animations: {
            self.shadowView.alpha = 1.0
            self.stackViewTransparent.alpha = 0.56
            self.sideViewTopConstraint.constant = 114
            self.sideViewBottomConstraint.constant = 114
            self.view.layoutIfNeeded()
        })
    }
    //
    func leftDidOpen() {
        self.shadowView.alpha = 1.0
        self.stackViewTransparent.alpha = 0.56
        self.sideViewBottomConstraint.constant = 114
        self.sideViewWidthConstraint.constant = 90
        self.sideViewTopConstraint.constant = 114
    }
    //
    func leftWillClose() {
        UIView.animate(withDuration: 0.5, animations: {
            self.shadowView.alpha = 0.0
            self.stackViewTransparent.alpha = 0.0
            self.sideViewBottomConstraint.constant = 0
            self.sideViewTopConstraint.constant = 0
            self.sideViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func leftDidClose() {
        
        self.shadowView.alpha = 0.0
        self.stackViewTransparent.alpha = 0.0
        self.sideViewBottomConstraint.constant = 0
        self.sideViewTopConstraint.constant = 0
        self.sideViewBottomConstraint.constant = 0
    }
}


//MARK: - PickerView Delegate & DataSource
extension SideMenuViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        statusList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return statusList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        status = statusList[row]
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
    }
}
