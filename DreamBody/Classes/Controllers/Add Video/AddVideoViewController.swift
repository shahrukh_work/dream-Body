//
//  AddViewController.swift
//  Dream-Body
//
//  Created by MAC on 21/10/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit
import AVFoundation


protocol Detail {
    var name: String {set get}
    var description: String {set get}
}

protocol VideoItem {
    var image: UIImage { get set }
    var isLastVideo: Bool { get set }
}

class AddVideo: Detail, VideoItem {
    var image: UIImage = UIImage()
    var isLastVideo: Bool = false
    var name: String = ""
    var description: String = ""
    
}

class AddVideoViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var saveButton: ActualGradientButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - Variables
    var isFirstTimePicked = true
    var videoDetail = AddVideo()
    var videoDetailArray: [AddVideo] = []
    var imagePicker = UIImagePickerController()
    var isEdit = false
    var videoUrl: URL?
    
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    
    //MARK: - Setup
    func setup () {
        tableView.delegate = self
        tableView.dataSource = self
        imagePicker.delegate = self
        videoDetail.name = "Lorem ipsumn name"
        videoDetail.description = "Lorem ipsumn Lorem ipsumn Lorem ipsumn Lorem ipsumn Lorem ipsumn"
        let item = AddVideo()
        item.isLastVideo = true
        
        if !isEdit {
            videoDetail.name = ""
            videoDetail.description = ""
            item.isLastVideo = false
        }
        
        item.image = #imageLiteral(resourceName: "confident-young-man-training-outdoors")
        
        videoDetailArray.append(videoDetail)
        videoDetailArray.append(item)

        
        if isEdit {
            let _ = videoDetailArray.map({$0.isLastVideo = false})
            let itemNew = AddVideo()
            itemNew.isLastVideo = true
            itemNew.image = #imageLiteral(resourceName: "confident-young-man-training-outdoors")
            headingLabel.text = "Edit Video"
            saveButton.setTitle("Save Changes", for: .normal)
            videoDetailArray.append(itemNew)
        }
        
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func savePressed(_ sender: Any) {
        
        if let url = videoUrl {
            APIClient.shared.uploadVideo(title: videoDetailArray[0].name, description: videoDetailArray[0].description, videoURL: videoUrl!) { message in
                
                print(message)
                
            }
        } else {
            self.showAlert(title: "", message: "Please select video from gallery.")
        }
        
    }
    
    
    //MARK: - Private Methods
    func openGallary () {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        imagePicker.mediaTypes = ["public.movie"]
        present(imagePicker, animated: true, completion: nil)
    }
}


//MARK: - UITableViewDelegate & UITableViewDatasource
extension AddVideoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        videoDetailArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.register(NameDescriptionTableViewCell.self, indexPath: indexPath)
            cell.config(data: videoDetailArray[indexPath.row])
            return cell
        }
        
        let cell = tableView.register(AddVideoTableViewCell.self, indexPath: indexPath)
        cell.thumbnailImageView.isHidden = false
        cell.thumbnailImageView.image = videoDetailArray[indexPath.row].image
        cell.headingLabel.text = ""

        cell.addVideoView.isHidden = true
        if videoDetailArray.count == 2 && isFirstTimePicked {
            cell.headingLabel.text = "Add Video"
            cell.addVideoView.isHidden = false
            cell.thumbnailImageView.isHidden = true

        } else {

            if indexPath.row == 1 {
                cell.headingLabel.text = "Add Video"

            } else {
                cell.headingLabel.text = ""
            }
        }
        
        cell.callBack = {
            let alert = UIAlertController(title: "Choose Video", message: nil, preferredStyle: .actionSheet)
          
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        return cell

    }
}


extension AddVideoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
           // profileImageView.image = image
          //  profileImage = image
            self.videoUrl = image
            let _ = videoDetailArray.map({$0.isLastVideo = false})
            let video = AddVideo()
            video.isLastVideo = true
            
            let asset = AVURLAsset(url: image , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            do {
                let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                video.image = thumbnail
                
                if isFirstTimePicked {
                    isFirstTimePicked.toggle()
                    videoDetailArray.removeLast()
                }
                
                videoDetailArray.append(video)
                tableView.reloadData()
            } catch {
                
            }
          

        }
        dismiss(animated: true, completion: nil)
    }
}
