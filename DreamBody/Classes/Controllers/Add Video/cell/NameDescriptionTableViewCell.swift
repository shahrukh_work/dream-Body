//
//  NameDescriptionTableViewCell.swift
//  Dream-Body
//
//  Created by MAC on 21/10/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class NameDescriptionTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var textContainerView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var data: AddVideo?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleTextField.setLeftPaddingPoints(10)
        titleTextField.delegate = self
        descriptionTextView.delegate = self
        
    }
    
    func config (data: AddVideo) {
        self.data = data
        if data.description == "" {
            descriptionTextView.text = "Description"
            descriptionTextView.textColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
            descriptionTextView.layer.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1).cgColor
            
        } else {
            descriptionTextView.text = data.description
            descriptionTextView.textColor = UIColor.black
            descriptionTextView.layer.borderColor = #colorLiteral(red: 0.07572624832, green: 0.08079931885, blue: 0.3107194006, alpha: 1).cgColor
        }
        
        if data.name != "" {
            titleTextField.text = data.name
            titleTextField.borderColor = #colorLiteral(red: 0.07572624832, green: 0.08079931885, blue: 0.3107194006, alpha: 1)
        }
    }
}


extension NameDescriptionTableViewCell: UITextViewDelegate {

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        data?.description = textView.text ?? "" + text
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if descriptionTextView.textColor == #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1) {
            descriptionTextView.text = ""
            descriptionTextView.textColor = UIColor.black
            descriptionTextView.layer.borderColor = #colorLiteral(red: 0.07572624832, green: 0.08079931885, blue: 0.3107194006, alpha: 1).cgColor
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        if descriptionTextView.text == "" {
            descriptionTextView.text = "Description"
            descriptionTextView.textColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
            descriptionTextView.layer.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1).cgColor
        }
    }
    
}


extension NameDescriptionTableViewCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        data?.name = textField.text ?? "" + string
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == titleTextField {
            titleTextField.borderColor = #colorLiteral(red: 0.07572624832, green: 0.08079931885, blue: 0.3107194006, alpha: 1)
            return
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == titleTextField {
            
            if textField.text?.isEmpty ?? false {
                titleTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
    }
}
