//
//  AddVideoTableViewCell.swift
//  Dream-Body
//
//  Created by MAC on 21/10/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class AddVideoTableViewCell: UITableViewCell {

    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var addVideoView: UIView!
    
    var callBack: (() -> ())?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func addVideoPressed(_ sender: Any) {
        self.callBack?()
    }
}
