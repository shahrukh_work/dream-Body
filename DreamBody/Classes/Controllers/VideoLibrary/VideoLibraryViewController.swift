//
//  VideoLibraryViewController.swift
//  Dream-Body
//
//  Created by MAC on 20/10/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class VideoLibraryViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - Variables
    var data: VideoLists?
    
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    
    //MARK: - Setup
    func setup () {
        tableView.delegate = self
        tableView.dataSource = self
        
        Utility.showLoading()
        VideoLists.loadData { (result, status) in
            Utility.hideLoading()
            if result != nil {
                self.data = result
                self.tableView.reloadData()
            }
        }
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.openLeft()
    }
    
    @IBAction func plusPressed(_ sender: Any) {
        let vc = AddVideoViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Private Methods
}


//MARK: - UITableViewDelegate & UITableViewDatasource
extension VideoLibraryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(VideoLibraryTableViewCell.self, indexPath:indexPath)
        cell.configure(data: (data?.data?[indexPath.row])!)
        cell.callBack = {
            let vc = AddVideoViewController()
            vc.isEdit = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.playCallBack = { link in
            
            let videoURL = URL(string: link.video_link ?? "")
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
            
        }
        
        cell.shareCallBack = { (link) in
            
            if let name = URL(string: link.video_link ?? ""), !name.absoluteString.isEmpty {
              let objectsToShare = [name]
              let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
              self.present(activityVC, animated: true, completion: nil)
                
            } else {
                self.showAlert(title: "", message: "Can not share link.")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
