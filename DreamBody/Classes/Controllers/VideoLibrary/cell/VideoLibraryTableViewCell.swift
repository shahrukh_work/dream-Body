//
//  VideoLibraryTableViewCell.swift
//  Dream-Body
//
//  Created by MAC on 20/10/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class VideoLibraryTableViewCell: UITableViewCell {

    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    var callBack: (() -> ())?
    var shareCallBack: ((_ data: VideoData) -> ())?
    var playCallBack: ((_ data: VideoData) -> ())?
    var data: VideoData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(data: VideoData) {
        self.data = data
        let result = Utility.dateAndTime(dataInString: data.post_date ?? "")
        titleLabel.text = data.post_title
        descriptionLabel.text = data.post_excerpt
        dateLabel.text = result?.0
        timeLabel.text = result?.1
    }
    
    @IBAction func editPressed(_ sender: Any) {
        self.callBack?()
    }
    
    @IBAction func sharePressed(_ sender: Any) {
        self.shareCallBack?(data!)
    }
    
    @IBAction func playPressed(_ sender: Any) {
        self.playCallBack?(data!)
    }
}
