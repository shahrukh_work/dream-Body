//
//  DayHistoryViewController.swift
//  Dream-Body
//
//  Created by MAC on 29/08/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit
import ObjectMapper

class Month {
    var name = ""
    var isSelected = false
    init(name: String, isSelected: Bool) {
        self.name = name
        self.isSelected = isSelected
    }
}

class DayHistoryViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerTextField: UITextField!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var noDataLabel: UILabel!
    
    
    var months = [Month]()
    var selectedMonth = "-1"
    var selectedMonthNumber = -1
    var selectedYear = "2021"
    var data = [History]()
    var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.noDataLabel.isHidden = true
        setup()

    }
    
    //MARK: - Setup
    func setup () {
        collectionView.delegate = self
        collectionView.dataSource = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        loadData()
        
    }
    
    func loadData () {
        let jan = Month(name: "Jan", isSelected: true)
        let feb = Month(name: "Feb", isSelected: false)
        let mar = Month(name: "Mar", isSelected: false)
        let apr = Month(name: "Apr", isSelected: false)
        let may = Month(name: "May", isSelected: false)
        let jun = Month(name: "Jun", isSelected: false)
        let jul = Month(name: "Jul", isSelected: false)
        let aug = Month(name: "Aug", isSelected: false)
        let sep = Month(name: "Sep", isSelected: false)
        let oct = Month(name: "Oct", isSelected: false)
        let nov = Month(name: "Nov", isSelected: false)
        let dec = Month(name: "Dec", isSelected: false)
        months = [jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec]
        self.pickerTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
        
        let dateformatter = DateFormatter() // 2-2
        dateformatter.dateFormat = "MM" // 2-3
        selectedMonth = dateformatter.string(from: Date()) //2-4
        selectedMonthNumber = Int(selectedMonth) ?? -1
        
        let dateformatter1 = DateFormatter() // 2-2
        dateformatter1.dateFormat = "yyyy" // 2-3
        selectedYear = dateformatter1.string(from: Date()) //2-4
        
        for (index,value) in months.enumerated() {
            value.isSelected = false
            
            if index == selectedMonthNumber - 1 {
                value.isSelected = true
                collectionView.reloadData()
            }
        }
        getFunction()
    }
    
    @objc func tapDone() {
        if let datePicker = self.pickerTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateFormat = "yyyy" // 2-3
            self.yearLabel.text = dateformatter.string(from: datePicker.date) //2-4
            selectedYear = dateformatter.string(from: datePicker.date)
        }
        self.pickerTextField.resignFirstResponder()
        getFunction ()// 2-5
    }
    
    @IBAction func openSideMenuPressed(_ sender: Any) {
        self.openLeft()
    }
    
    @IBAction func datePickerOpenPressed(_ sender: Any) {
        pickerTextField.becomeFirstResponder()
    }
    
    func getFunction () {
        
        isLoading = true
        data.removeAll()
        ChartHistory.getHistory(year: selectedYear, month: selectedMonth) { (result, status) in
            self.data = result ?? []
            self.isLoading = false
            self.noDataLabel.isHidden = true
            
            if self.data.count == 0 {
                self.noDataLabel.isHidden = false
            }
            self.tableView.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                Utility.takeScreenShot(view: self.view)
            }
        } _: { (error, status) in
            self.showAlert(title: "", message: error?.localizedDescription ?? "")
        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDatasource
extension DayHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if data.count == 0 && isLoading {
            return 4
        }
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(LBSTableViewCell.self, indexPath: indexPath)
        
        if data.count == 0 && isLoading {
            cell.showHideSkeleton(show: true)
            
        } else {
            cell.showHideSkeleton(show: false)
            cell.configure(data: data[indexPath.row])
        }
        cell.frame = tableView.bounds
        return cell
    }
}


//MARK: - UICollectionViewDelegate & UICollectionViewDatasource
extension DayHistoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        months.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.register(DayCollectionViewCell.self, indexPath: indexPath)
        cell.configure(month: months[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        months.map({$0.isSelected = false})
        months[indexPath.row].isSelected = true
        selectedMonth = "\(indexPath.row + 1)"
        collectionView.reloadData()
        getFunction ()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 35)
    }
}

