//
//  DayCollectionViewCell.swift
//  Dream-Body
//
//  Created by MAC on 29/08/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class DayCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var barView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure (month: Month) {
        barView.backgroundColor = .clear
        dayLabel.text = month.name
        
        if month.isSelected {
            barView.backgroundColor = #colorLiteral(red: 0.6574038863, green: 0.7485360503, blue: 0.1935526133, alpha: 1)
        }
    }
}
