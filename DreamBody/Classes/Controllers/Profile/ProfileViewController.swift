//
//  ProfileViewController.swift
//  Dream-Body
//
//  Created by MAC on 26/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit
import SDWebImage

extension UITextField {
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        datePicker.maximumDate = Date()
        // iOS 14 and above
        if #available(iOS 14, *) {// Added condition for iOS 14
          datePicker.preferredDatePickerStyle = .wheels
          datePicker.sizeToFit()
        }
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
}

class ProfileViewController: UIViewController {
   
    
    @IBOutlet weak var saveButton: ActualGradientButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    
    
    var imagePicker = UIImagePickerController()
    var profileImage: UIImage?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        emailTextField.layoutSkeletonIfNeeded()
        firstNameTextField.layoutSkeletonIfNeeded()
        lastNameTextField.layoutSkeletonIfNeeded()
        saveButton.layoutSkeletonIfNeeded()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.showHideSkeleton(show: true)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.showHideSkeleton(show: false)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            Utility.takeScreenShot(view: self.view)
        }
        
    }
    
    func setupView () {
        firstNameTextField.text = DataManager.shared.getUserDetail()?.displayName
        lastNameTextField.text = DataManager.shared.getUserDetail()?.name
        imagePicker.delegate = self
        emailTextField.setLeftPaddingPoints(10)
        lastNameTextField.setLeftPaddingPoints(10)
        firstNameTextField.setLeftPaddingPoints(10)
        emailTextField.text = DataManager.shared.getUserDetail()?.dob
        firstNameTextField.delegate = self
        emailTextField.delegate = self
        lastNameTextField.delegate = self
        Utility.takeScreenShot(view: self.view)
        profileImageView.sd_setImage(with: URL(string: DataManager.shared.getUserDetail()?.image ?? ""), placeholderImage: #imageLiteral(resourceName: "profile_image"))
        self.emailTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
       

    }
    
    @objc func tapDone() {
        if let datePicker = self.emailTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateFormat = "yyyy-MM-dd" // 2-3
            self.emailTextField.text = dateformatter.string(from: datePicker.date) //2-4
        }
        self.emailTextField.resignFirstResponder() // 2-5
    }
    
    @IBAction func backPressed(_ sender: Any) {
        slideMenuController()?.openLeft()
    }
    
    @IBAction func editProfileImagePressed(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func resetPasswordPressed(_ sender: Any) {
        let vc = ResetPasswordViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func saveChangesPressed(_ sender: Any) {
        
        if emailTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "DOB Field is empty!")
            return
        }
        
        if firstNameTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "First Name Field is empty!")
            return
        }
        
        if lastNameTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Last Name Field is empty!")
            return
        }
        
        APIClient.shared.updateProfileData(firstName: firstNameTextField.text ?? "", lastName: lastNameTextField.text ?? "",dob: emailTextField.text ?? "", image: profileImage) { message in
            self.showAlert(title: "", message: message!)
            
            UserDetail.userDetail { (result, status) in
                Utility.hideLoading()
                
            } _: { (error, status) in
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
    
    func showHideSkeleton (show: Bool) {
        profileImageView.isSkeletonable = true
        emailTextField.isSkeletonable = true
        lastNameTextField.isSkeletonable = true
        firstNameTextField.isSkeletonable = true
        
        saveButton.isSkeletonable = true
        headingLabel.isSkeletonable = true
        resetButton.isSkeletonable = true
        editButton.isSkeletonable = true
        
        
        if show {
            profileImageView.showGradientSkeleton()
            emailTextField.showGradientSkeleton()
            lastNameTextField.showGradientSkeleton()
            firstNameTextField.showGradientSkeleton()
        
            saveButton.showGradientSkeleton()
            headingLabel.showGradientSkeleton()
            resetButton.showGradientSkeleton()
            editButton.showGradientSkeleton()

        } else {
            profileImageView.hideSkeleton()
            emailTextField.hideSkeleton()
            lastNameTextField.hideSkeleton()
            firstNameTextField.hideSkeleton()
            
            saveButton.hideSkeleton()
            headingLabel.hideSkeleton()
            resetButton.hideSkeleton()
            editButton.hideSkeleton()
        }
    }
    
}


extension ProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailTextField {
            emailTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
        
        if textField == lastNameTextField {
            lastNameTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
        
        if textField == firstNameTextField {
            firstNameTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == emailTextField {
            
            if textField.text?.isEmpty ?? false {
                emailTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
        if textField == lastNameTextField {
            
            if textField.text?.isEmpty ?? false {
                lastNameTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
        if textField == firstNameTextField {
            
            if textField.text?.isEmpty ?? false {
                firstNameTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImageView.image = image
            profileImage = image
        }
        dismiss(animated: true, completion: nil)
    }
}

