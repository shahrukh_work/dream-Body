//
//  WeightLossViewController.swift
//  Dream-Body
//
//  Created by MAC on 27/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit
import AVKit

enum Options {
    case videos
    case links
}

class WeightLossViewController: UIViewController {

    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var linksButton: UIButton!
    @IBOutlet weak var videosButton: UIButton!
    @IBOutlet weak var videosBottomView: UIView!
    @IBOutlet weak var linksBottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    var mode: Options = .videos
    var weightInfo: [WeightInfo] = []
    var isFavorite: Bool?
    var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  setupView()
        // Do any additional setup after loading the view.
        if self.isFavorite ?? false {
            headingLabel.text = "Favorites"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        noDataLabel.isHidden = true
    }

    
    private func setupView() {
        tableView.dataSource = self
        tableView.delegate = self
        Utility.takeScreenShot(view: self.view)
        loadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        isLoading = false
    }
    
    //MARK: - Private Methods
    func loadData () {
        isLoading = true
        self.weightInfo.removeAll()
        tableView.reloadData()
        WeightInfo.getWeightInfoList(isFav: isFavorite) { (result, status) in
            self.weightInfo = result ?? []
            self.isLoading = false
            
            if self.weightInfo.count == 0 {
                self.noDataLabel.isHidden = false
            }
            self.tableView.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                Utility.takeScreenShot(view: self.view)
            }
            
        } _: { (err, status) in
            self.showAlert(title: "", message: err?.localizedDescription ?? "")
        }
    }
    

    @IBAction func videosButtonTapped(_ sender: Any) {
        linksBottomView.backgroundColor = .clear
        linksBottomView.alpha = 0.2
        linksButton.alpha = 0.3
        videosBottomView.alpha = 1.0
        videosButton.alpha = 1.0
        videosBottomView.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.6470588235, blue: 0.2352941176, alpha: 1)
        mode = .videos
        tableView.reloadData()
    }
    
    @IBAction func linksButtonTapped(_ sender: Any) {
        linksBottomView.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.6470588235, blue: 0.2352941176, alpha: 1)
        linksBottomView.alpha = 1.0
        linksButton.alpha = 1.0
        videosBottomView.alpha = 0.2
        videosButton.alpha = 0.3
        videosBottomView.backgroundColor = .clear
        mode = .links
        tableView.reloadData()
    }
    
    @IBAction func hamburgerPressed(_ sender: Any) {
        self.openLeft()
    }

}


//MARK: - UITableViewDelegate & UITableViewDatasource
extension WeightLossViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if weightInfo.count == 0 && isLoading {
            return 4
        }
        return weightInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if mode == .videos {
            let cell = tableView.register(VideoTableViewCell.self, indexPath: indexPath)

            if weightInfo.count == 0 && isLoading {
                cell.showHideSkeleton(show: true)
                
            } else {
                cell.showHideSkeleton(show: false)
                cell.configure(data: weightInfo[indexPath.row])
            }
            cell.closureBack = { url in
                let items = [URL(string: url)!]
                let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                self.present(ac, animated: true)
            }
            return cell
            
        } else {
            let cell = tableView.register(LinksTableViewCell.self, indexPath: indexPath)
            cell.configure(data: self.weightInfo[indexPath.row])
            
            cell.closureBack = { url in
                let items = [URL(string: url)!]
                let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                self.present(ac, animated: true)
            }
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let videoURL = URL(string: self.weightInfo[indexPath.row].meta_value ?? "")
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
}
