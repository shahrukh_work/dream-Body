//
//  LinksTableViewCell.swift
//  Dream-Body
//
//  Created by MAC on 27/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class LinksTableViewCell: UITableViewCell {

    @IBOutlet weak var videoDescriptionsLabel: UILabel!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var urlVideoLabel: UILabel!
    
    var isFavorite = false
    var weight: WeightInfo?
    var closureBack: ((_ url: String) -> ())?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func configure (data: WeightInfo) {
        self.weight = data
        videoTitleLabel.text = data.post_title
        urlVideoLabel.text = data.meta_value
        videoDescriptionsLabel.text = data.post_excerpt
        isFavorite = data.isFav
        
        if data.isFav {
            favButton.setImage(#imageLiteral(resourceName: "fav_selected"), for: .normal)
            
        } else {
            favButton.setImage(#imageLiteral(resourceName: "fav_unselected"), for: .normal)
        }
    }

    @IBAction func copyLinkPressed(_ sender: Any) {
        //UIPasteboard.general.string = urlVideoLabel.text
        guard let url = URL(string: urlVideoLabel.text ?? "") else { return }
        UIApplication.shared.open(url)

    }
    
    @IBAction func sharePressed(_ sender: Any) {
        self.closureBack?(urlVideoLabel.text ?? "")
    }
    
    @IBAction func favPressed(_ sender: Any) {
        
        if isFavorite {
            isFavorite = !isFavorite
            setFavorite(isFav: false)
            favButton.setImage(#imageLiteral(resourceName: "fav_unselected"), for: .normal)

        } else {
            isFavorite = !isFavorite
            favButton.setImage(#imageLiteral(resourceName: "fav_selected"), for: .normal)
            setFavorite(isFav: true)
        }
    }
    
    func setFavorite (isFav: Bool) {
        APIClient.shared.favoriteMethod(postID: weight?.post_id ?? "", key: weight?.meta_key ?? "", isFav: isFav) { (result, error, status) in
        }
    }
}
