//
//  VideoTableViewCell.swift
//  Dream-Body
//
//  Created by MAC on 27/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation

class VideoTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImageview: UIImageView!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var favButton: UIButton!
    var isFavorite = false
    
    var weight: WeightInfo?
    var closureBack: ((_ url: String) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func configure (data: WeightInfo) {
        self.weight = data
        videoTitleLabel.text = data.post_title
        linkLabel.text = data.meta_value
        descriptionLabel.text = data.post_excerpt
        isFavorite = data.isFav
        
        if data.isFav {
            favButton.setImage(#imageLiteral(resourceName: "fav_selected"), for: .normal)
        } else {
            
            favButton.setImage(#imageLiteral(resourceName: "fav_unselected"), for: .normal)
        }
        self.itemImageview.sd_setImage(with: URL(string: data.thumbNail), placeholderImage:  #imageLiteral(resourceName: "white-woman-posing-with-measure-tape-her-waist"))
    }
    
    @IBAction func linkPressed(_ sender: Any) {
        guard let url = URL(string: linkLabel.text ?? "") else { return }
        UIApplication.shared.open(url)

    }
    
    @IBAction func sharePressed(_ sender: Any) {
        self.closureBack?(linkLabel.text ?? "")
    }
    
    
    @IBAction func favPressed(_ sender: Any) {
        
        if isFavorite {
            isFavorite = !isFavorite
            setFavorite(isFav: false)
            favButton.setImage(#imageLiteral(resourceName: "fav_unselected"), for: .normal)

        } else {
            isFavorite = !isFavorite
            favButton.setImage(#imageLiteral(resourceName: "fav_selected"), for: .normal)
            setFavorite(isFav: true)
        }
    }
    
    func setFavorite (isFav: Bool) {
        APIClient.shared.favoriteMethod(postID: weight?.post_id ?? "", key: weight?.meta_key ?? "", isFav: isFav) { (result, error, status) in
        }
    }
    
    func showHideSkeleton (show: Bool) {
        itemImageview.isSkeletonable = true
        linkLabel.isSkeletonable = true
        descriptionLabel.isSkeletonable = true
        videoTitleLabel.isSkeletonable = true
        shareButton.isSkeletonable = true
        favButton.isSkeletonable = true
        
        if show {
            itemImageview.showGradientSkeleton()
            linkLabel.showGradientSkeleton()
            descriptionLabel.showGradientSkeleton()
            videoTitleLabel.showGradientSkeleton()
            shareButton.showGradientSkeleton()
            favButton.showGradientSkeleton()
            
        } else {
            itemImageview.hideSkeleton()
            linkLabel.hideSkeleton()
            descriptionLabel.hideSkeleton()
            videoTitleLabel.hideSkeleton()
            shareButton.hideSkeleton()
            favButton.hideSkeleton()
            
        }
    }
}
