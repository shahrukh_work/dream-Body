//
//  ResetPasswordViewController.swift
//  Dream-Body
//
//  Created by MAC on 26/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    
    @IBOutlet weak var currentTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView () {
        currentTextField.setLeftPaddingPoints(10)
        newPasswordTextField.setLeftPaddingPoints(10)
        confirmPasswordTextField.setLeftPaddingPoints(10)
        confirmPasswordTextField.delegate = self
        currentTextField.delegate = self
        newPasswordTextField.delegate = self
    }
    
    @IBAction func resetPasswordPressed(_ sender: Any) {
        if currentTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Current Password Field is empty!")
            return
        }
        
        if newPasswordTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "New Password Field is empty!")
            return
        }
        
        if confirmPasswordTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Confirm Password Field is empty!")
            return
        }
        
        if currentTextField.text?.count ?? 0 < 6  {
            self.showAlert(title: "", message: "Password Length should be 6!")
            return
        }
        
        if newPasswordTextField.text?.count ?? 0 < 6  {
            self.showAlert(title: "", message: "Password Length should be 6!")
            return
        }
        
        if confirmPasswordTextField.text?.count ?? 0 < 6  {
            self.showAlert(title: "", message: "Password Length should be 6!")
            return
        }
        
        if newPasswordTextField.text ?? "" != confirmPasswordTextField.text ?? ""  {
            self.showAlert(title: "", message: "Password does not match!")
            return
        }
    
        self.showAlert(title: "", message: "Password Successfully reset.")
        
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension ResetPasswordViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == newPasswordTextField {
            newPasswordTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
        
        if textField == currentTextField {
            currentTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
        
        if textField == confirmPasswordTextField {
            confirmPasswordTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == newPasswordTextField {
            
            if textField.text?.isEmpty ?? false {
                newPasswordTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
        if textField == currentTextField {
            
            if textField.text?.isEmpty ?? false {
                currentTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
        if textField == confirmPasswordTextField {
            
            if textField.text?.isEmpty ?? false {
                confirmPasswordTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
    }
}
