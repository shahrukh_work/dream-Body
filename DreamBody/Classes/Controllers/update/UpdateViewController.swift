//
//  UpdateViewController.swift
//  Dream-Body
//
//  Created by MAC on 14/09/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class UpdateViewController: UIViewController {
    
    
    //MARK: - Outlet
    @IBOutlet weak var enterValueTextField: UITextField!
    
    
    //MARK: - Variables
    var postId = ""
    var key = ""
    var value = ""
    var callBack: (() -> ())?
    
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    
    //MARK: - Setup
    func setup () {
        enterValueTextField.setLeftPaddingPoints(10)
        enterValueTextField.delegate = self
    }
    
    
    //MARK: - Actions
    @IBAction func updatePressed(_ sender: Any) {
        
        if enterValueTextField.text == "" {
            self.showAlert(title: "", message: "Value field is empty!")
            return
        }
        value = enterValueTextField.text ?? ""
        
        Utility.showLoading()
        APIClient.shared.updateValue(postId: postId, key: key, value: value) { (result, error, status) in
            Utility.hideLoading()
        print(result)
            if error == nil {
                if let newResult = result as? [String: Any] {
                    
                    if let success = newResult["success"] as? Bool {
                        
                        if !success {
                            self.showAlert(title: "Error", message:  (newResult["message"] as? String) ?? "")
                            
                            return
                        } else {
                            self.callBack?()
                        }
                    }
                }
                self.dismiss(animated: true, completion: nil)
                
            } else {
                self.showAlert(title: "error", message: error?.localizedDescription ?? "")
            }
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Private Methods
}

extension UpdateViewController: UITextFieldDelegate {
    
    func UpdateViewController(_ textField: UITextField) {
        
        if textField == enterValueTextField {
            enterValueTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == enterValueTextField {
            
            if textField.text?.isEmpty ?? false {
                enterValueTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
    }
}

