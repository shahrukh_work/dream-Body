//
//  LoginViewController.swift
//  Dream-Body
//
//  Created by MAC on 26/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class ActualGradientButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundColor = #colorLiteral(red: 0.8431372549, green: 0.2, blue: 0.5058823529, alpha: 1)
        //gradientLayer.frame = bounds
    }
}

class LoginViewController: UIViewController {
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextFeidl: UITextField!
    @IBOutlet weak var loginButton: ActualGradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupView()
    }
    
    func setupView () {
        emailTextFeidl.setLeftPaddingPoints(10)
        passwordTextField.setLeftPaddingPoints(10)
        emailTextFeidl.delegate = self
        passwordTextField.delegate = self
    }
    

    //MARK: - Actions
    @IBAction func loginPressed(_ sender: Any) {
        
        if emailTextFeidl.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Email Field is empty!")
            return
        }
        
        if passwordTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Password Field is empty!")
            return
        }
        
//        if (passwordTextField.text?.count ?? 0) < 6  {
//            self.showAlert(title: "", message: "Password Length should be 6!")
//            return
//        }
        
        Utility.showLoading()
        
        Login.login(email: emailTextFeidl.text!, password: passwordTextField.text!) { (result, status) in
            
            UserDetail.userDetail { (result, status) in
                Utility.hideLoading()
                
                if result?.userType == "administrator" {
                    Utility.setupAdminViewController()
                    return
                }
                
                if (result?.phoneNumberVerification ?? "") == "1" {
                    Utility.setupHomeAsRootViewController()
                    
                } else {
                    let vc = PhoneNumberViewController()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            } _: { (error, status) in
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }

        } _: { (error, status, message) in
            Utility.hideLoading()
            if status == 403 {
                self.showAlert(title: "", message: "Please check you email or password.")
                return
            }
            self.showAlert(title: "", message: error?.localizedDescription ?? "")
        }

    }
    
    @IBAction func forgotPressed(_ sender: Any) {
        let vc = ForgotPasswordViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signUpPressed(_ sender: Any) {
        
        Login.login(email: "Code", password: "C0d3@DreamB0D9sYstem") { (result, status) in
            
        } _: { (error, status, message)  in
            self.showAlert(title: "", message: error?.localizedDescription ?? "")
        }
        let vc = SignUpViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailTextFeidl {
            emailTextFeidl.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
        
        if textField == passwordTextField {
            passwordTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == emailTextFeidl {
            
            if textField.text?.isEmpty ?? false {
                emailTextFeidl.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
        if textField == passwordTextField {
            
            if textField.text?.isEmpty ?? false {
                passwordTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
    }
}
 
