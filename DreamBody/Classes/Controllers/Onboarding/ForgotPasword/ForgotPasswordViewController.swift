//
//  ForgotPasswordViewController.swift
//  Dream-Body
//
//  Created by MAC on 26/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.setLeftPaddingPoints(10)
        emailTextField.delegate = self
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        if emailTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Email Field is empty!")
            return
        }
        
        if !(emailTextField.text?.isValidEmail ?? false) {
            self.showAlert(title: "", message: "Email is invalid!")
            return
        }
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailTextField {
            emailTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == emailTextField {
            
            if textField.text?.isEmpty ?? false {
                emailTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
    }
}
