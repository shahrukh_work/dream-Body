//
//  SignUpViewController.swift
//  Dream-Body
//
//  Created by MAC on 26/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var last: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView () {
        firstNameTextField.setLeftPaddingPoints(10)
        last.setLeftPaddingPoints(10)
        emailTextField.setLeftPaddingPoints(10)
        passwordTextField.setLeftPaddingPoints(10)
        confirmPassword.setLeftPaddingPoints(10)
        
        firstNameTextField.delegate = self
        last.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPassword.delegate = self
    }
    
    @IBAction func signUpPressed(_ sender: Any) {
        if emailTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Email Field is empty!")
            return
        }
        
        if !(emailTextField.text?.isValidEmail ?? false) {
            self.showAlert(title: "", message: "Email Field is empty!")
            return
        }
        
        if passwordTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Password Field is empty!")
            return
        }
        
        if firstNameTextField.text?.isValidEmail ?? false {
            self.showAlert(title: "", message: "First Name Field is empty!")
            return
        }
        
        if last.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Last Name Field is empty!")
            return
        }
        
        if passwordTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Password Field is empty!")
            return
        }
        
        if confirmPassword.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Confirm Password Field is empty!")
            return
        }
        
//        if (passwordTextField.text?.count ?? 0) < 6  {
//            self.showAlert(title: "", message: "Password Length should be 6!")
//            return
//        }
//
//        if confirmPassword.text?.count ?? 0 < 6  {
//            self.showAlert(title: "", message: "Password Length should be 6!")
//            return
//       }
        
        if passwordTextField.text ?? "" != confirmPassword.text ?? ""  {
            self.showAlert(title: "", message: "Password does not match!")
            return
        }
        
        User.signUp(userName: firstNameTextField.text!, name: last.text!, email: emailTextField.text!, password: confirmPassword.text!) { (result, status) in
            
            Login.login(email: self.emailTextField.text!, password: self.confirmPassword.text!) { (result, status) in

                UserDetail.userDetail { (result, status) in
                    Utility.hideLoading()
                    let vc = PhoneNumberViewController()
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } _: { (error, status) in
                    self.showAlert(title: "", message: error?.localizedDescription ?? "")
                }

            } _: { (error, status, message)  in
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
            
            
        } _: { (error, status) in
            self.showAlert(title: "", message: error?.localizedDescription ?? "")
        }
    }
    
    @IBAction func signInPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == firstNameTextField {
            firstNameTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
        
        if textField == last {
            last.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
        
        if textField == emailTextField {
            emailTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
        
        if textField == passwordTextField {
            passwordTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
        
        if textField == confirmPassword {
            confirmPassword.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == firstNameTextField {
            
            if textField.text?.isEmpty ?? false {
                firstNameTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
        if textField == last {
            
            if textField.text?.isEmpty ?? false {
                last.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
        if textField == emailTextField {
            
            if textField.text?.isEmpty ?? false {
                emailTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
        if textField == passwordTextField {
            
            if textField.text?.isEmpty ?? false {
                passwordTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
        if textField == confirmPassword {
            
            if textField.text?.isEmpty ?? false {
                confirmPassword.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
        
    }
}

