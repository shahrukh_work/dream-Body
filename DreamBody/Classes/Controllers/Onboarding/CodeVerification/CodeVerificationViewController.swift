//
//  CodeVerificationViewController.swift
//  Krafty
//
//  Created by Mian Faizan Nasir on 4/23/20.
//  Copyright © 2020 Mian Faizan Nasir. All rights reserved.
//

import UIKit
import FirebaseAuth


class CodeVerificationViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var backButton: UIButton!
  //  @IBOutlet weak var ringImageView: UIImageView!
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    @IBOutlet weak var textField6: UITextField!
    @IBOutlet weak var textField5: UITextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var verifyButton: UIButton!
   // @IBOutlet weak var phoneNumberLabel: UILabel!
    
    
    //MARK: - Variables
    var email = ""
    var verificationPin = ""
    var phoneNumber = ""
    var code = ""
 //   var signup: AuthSignup!
    
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    //MARK: - View Setup
    private func setupView() {
        setUIOrientation()
        
        textField1.delegate = self
        textField2.delegate = self
        textField3.delegate = self
        textField4.delegate = self
        textField5.delegate = self
        textField6.delegate = self
        
        textField2.isUserInteractionEnabled = false
        textField3.isUserInteractionEnabled = false
        textField4.isUserInteractionEnabled = false
        textField5.isUserInteractionEnabled = false
        textField6.isUserInteractionEnabled = false
        
        textField1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField2.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField3.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField4.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField5.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField6.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        (self.view.viewWithTag(1001) as! UITextField).becomeFirstResponder()
        
        verifyButton.isUserInteractionEnabled = false
        //verifyButton.backgroundColor = #colorLiteral(red: 0.996422708, green: 0.5768802762, blue: 0.3608906269, alpha: 0.7378088338)
        
        Utility.setPlaceHolderTextColor(textField1, "-", UIColor.black)
        Utility.setPlaceHolderTextColor(textField2, "-", UIColor.black)
        Utility.setPlaceHolderTextColor(textField3, "-", UIColor.black)
        Utility.setPlaceHolderTextColor(textField4, "-", UIColor.black)
        Utility.setPlaceHolderTextColor(textField5, "-", UIColor.black)
        Utility.setPlaceHolderTextColor(textField6, "-", UIColor.black)
        
        contentView.cornerRadius = 19
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
    
    //MARK: - Actions
    @IBAction func verifyButtonTapped(_ sender: Any) {
        var verificationCode = textField1.text! + textField2.text! + textField3.text!
        verificationCode = verificationCode + textField4.text! + textField5.text! + textField6.text!
       // Utility.setupHomeAsRootViewController()
        
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")!
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode)

        Utility.showLoading()
        Auth.auth().signIn(with: credential) { (authResult, error) in

            Utility.hideLoading()
            if let error = error {
                print(error.localizedDescription)
                self.verifyButton.isUserInteractionEnabled = false
                self.verifyButton.alpha = 0.5
               // self.showSuccessErrorPrompt("Verification Code is Invalid", mode: .error)
                return
            }

            if let result = authResult {

                Utility.showLoading()
                result.user.getIDTokenForcingRefresh(true) { (token, error) in

                    if error == nil {
                        
                        UserDetail.verifyPhoneNumber(isVerified: 1) { (result, status) in
                            Utility.hideLoading()
                            DataManager.shared.setFirstTimSignUp(enable: true)
                            Utility.setupHomeAsRootViewController()
                            
                        } _: { (error, status) in
                            self.showAlert(title: "", message: error?.localizedDescription ?? "")
                        }

                    }
                }
            }
        }
    }
    
    @IBAction func editMobileNumberButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendCodeButtonTapped(_ sender: Any) {
        Utility.showLoading()
        
        PhoneAuthProvider.provider().verifyPhoneNumber(code+phoneNumber, uiDelegate: nil) { (verificationID, error) in
            
            Utility.hideLoading()
            
            if let error = error {
                print("Authentication error!", error.localizedDescription)
                return
            }
            
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            self.showAlert(title: "", message: "Verification Code sent you phone number.")
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - UITextFieldDelegate
    @objc func textFieldDidChange(_ textField: UITextField) {
        let tag = textField.tag
        textField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
        textField.borderWidth = 1.0
        if tag == 1006 {
            
            var verificationCode = textField1.text! + textField2.text! + textField3.text!
            verificationCode = verificationCode + textField4.text! + textField5.text! + textField6.text!
            
            if verificationCode.count == 6 {
                verifyButton.isUserInteractionEnabled = true
                verifyButton.alpha = 1
                
            } else {
                verifyButton.isUserInteractionEnabled = false
                verifyButton.alpha = 0.5
            }
            
        } else {
            let text = textField.text!
            
            if text != "" {
                let lastChar = String(describing: Array(text)[text.count - 1])
                textField.text = lastChar + ""
                (self.view.viewWithTag(tag+1) as! UITextField).isUserInteractionEnabled = true
                (self.view.viewWithTag(tag+1) as! UITextField).becomeFirstResponder()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            textField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
            textField.borderWidth = 1.0
            
            verifyButton.isUserInteractionEnabled = false
            verifyButton.alpha = 0.5
            textField.isUserInteractionEnabled =  true
            textField.text = ""
           // invalidCodeLabel.isHidden = true
            
            if textField.tag != 1001
            {
                (self.view.viewWithTag(textField.tag-1) as! UITextField).becomeFirstResponder()
                return false
            }
        }
        let bool = maxLengthForTextfield(textfield: textField, range: range, string: string, length: 1)
        return true && bool
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func maxLengthForTextfield(textfield: UITextField, range: NSRange, string: String, length: Int) -> Bool {
        let currentText = textfield.text ?? ""
        guard let strRange = Range(range, in: currentText) else { return false }
        let newString = currentText.replacingCharacters(in: strRange, with: string)
        
        return newString.count <= length
    }
    
    private func setUIOrientation() {
//        if Localize.currentLanguage() == "ar" {
//            backButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
//          //  ringImageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
//        }
    }
}
