//
//  PhoneNumberViewController.swift
//  Dream-Body
//
//  Created by MAC on 26/06/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit
import FirebaseAuth

class PhoneNumberViewController: UIViewController {
    @IBOutlet weak var dailCodeLae: UILabel!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTextField.delegate = self
        phoneNumberTextField.setLeftPaddingPoints(10)
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        
        if phoneNumberTextField.text?.isEmpty ?? false {
            self.showAlert(title: "", message: "Phone number Field is empty!")
            return
        }
        
        Utility.showLoading()
        PhoneAuthProvider.provider().verifyPhoneNumber(dailCodeLae.text!+phoneNumberTextField.text!, uiDelegate: nil) { (verificationID, error) in
            
            Utility.hideLoading()
            
            if let error = error {
                self.showAlert(title: "", message: error.localizedDescription)
                return
            }
            
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            print("!!!! verification code sent successfully !!!!")
            
            let verifyCodeViewController = CodeVerificationViewController()
            verifyCodeViewController.modalPresentationStyle = .fullScreen
            verifyCodeViewController.phoneNumber = self.phoneNumberTextField.text ?? ""
            verifyCodeViewController.code = self.dailCodeLae.text!
            self.navigationController?.pushViewController(verifyCodeViewController, animated: true)
        }
    }
    
    @IBAction func selectCountryPressed(_ sender: Any) {
        let selectCountryViewController = SelectCountryViewController()
        selectCountryViewController.delegate = self
        selectCountryViewController.modalPresentationStyle = .fullScreen
        self.present(selectCountryViewController, animated: true, completion: nil)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension PhoneNumberViewController: SelectCountryViewControllerDelegate {
    func didSelectCuontry(country: Country) {
        dailCodeLae.text = country.dial_code
    }
}

extension PhoneNumberViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneNumberTextField {
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            if newString.length > maxLength {
                return false
            }
            
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == phoneNumberTextField {
            phoneNumberTextField.borderColor = #colorLiteral(red: 0.1974356174, green: 0.3847657144, blue: 0.6401544809, alpha: 1)
            return
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == phoneNumberTextField {
            
            if textField.text?.isEmpty ?? false {
                phoneNumberTextField.borderColor = #colorLiteral(red: 0.7365563512, green: 0.7630630136, blue: 0.8376937509, alpha: 1)
                return
            }
        }
    }
}
