//
//  MessagingViewController.swift
//  Dream-Body
//
//  Created by MAC on 11/08/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit


class MessagingViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {

    
    //MARK: - Outlet
    @IBOutlet weak var endChatButton: UIButton!
    @IBOutlet weak var typeTextArea: UITextView!
    @IBOutlet weak var actualMessageLabel: UITextView!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - Variables
    var userId = ""
    var message: [Message] = []
    var isFromBuzz = false
    var isAdmin = false
    var numberOfPages = 1
    var offset = 20
    var isFirstTime = true
    
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        if isFromBuzz {
            endChatButton.isHidden = true
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        allMessages()

    }
    
    //MARK: - Setup
    func setup () {
        tableView.delegate = self
        tableView.dataSource = self
        dummyMeessage()
        typeTextArea.text = "Type a message"
        typeTextArea.textColor = UIColor.lightGray
        typeTextArea.delegate = self
    }
    
    
    //MARK: - Actions
    @IBAction func backPressed(_ sender: Any) {
        self.openLeft()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        isFirstTime = true
        let user = DataManager.shared.getUserDetail()
        if isAdmin {
            
            ChatManager.shared.sendMessage(userId: userId, withMessage: typeTextArea.text ?? "") { status, error in
                
                if error == nil {
                    self.typeTextArea.text = ""
                }
            }
            
            ChatManager.shared.sendUpdateLatestByAdmin(userId: userId, withMessage: typeTextArea.text ?? "") { result, error in
                
                if error == nil {
                    
                }
            }
            
        } else {
            ChatManager.shared.sendMessage(userId: userId, type: userId, withMessage: typeTextArea.text ?? "") { status, error in
                
                if error == nil {
                    self.typeTextArea.text = ""
                }
            }
            
            ChatManager.shared.sendUpdateLatestByUser(name: user?.displayName ?? "", userId: userId, withMessage: typeTextArea.text ?? "", imageUrl: DataManager.shared.getUserDetail()?.image ?? "") { status, error in
                
                if error == nil {
                    
                }
            }
        }
    }
    
    @IBAction func endChatPressed(_ sender: Any) {
      //  navController = self.navigationController!
//        let vc = HelpfulRecommendationViewController()
//        vc.closure = {
//            let vc = RateAndTipViewController()
//            vc.modalPresentationStyle = .overCurrentContext
//            self.navigationController?.present(vc, animated: true, completion: nil)
//        }
//        vc.modalPresentationStyle = .overCurrentContext
//        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //MARK: - Private Methods
    func allMessages () {
       
        ChatManager.shared.fetchUserChats(offset: offset, userId: userId, completion: { message, error in
            Utility.hideLoading()
            
            if error == nil {
                self.message = message!
                self.tableView.reloadData()
                
                if self.isFirstTime {
                    self.isFirstTime = false
                    self.scrollToEnd()
                }
            }
        })
    }
    
    func dummyMeessage () {
  //      message.append(contentsOf: [ ChatMessage(name: "John", body: "hello how are you", date: "08:53", type: .sender), ChatMessage(name: "John", body: "hello how are you", date: "08:53", type: .receiver), ChatMessage(name: "John", body: "hello how are you", date: "08:53", type: .receiver), ChatMessage(name: "John", body: "hello how are you", date: "08:53", type: .sender)])
        
    }
    
    func scrollToEnd() {
        if message.count != 0 {
            tableView.scrollToRow(at: IndexPath(row: message.count - 1, section: 0), at: .bottom, animated: false)

        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if typeTextArea.textColor == UIColor.lightGray {
            typeTextArea.text = ""
            typeTextArea.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        if typeTextArea.text == "" {

            typeTextArea.text = "Type a message"
            typeTextArea.textColor = UIColor.lightGray
        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDatasource
extension MessagingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        message.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if message[indexPath.row].uid != userId {
            
            if !isAdmin {
                let cell = tableView.register(SenderTableViewCell.self, indexPath: indexPath)
                cell.configure(message: message[indexPath.row])
                return cell
            } else {
                let cell = tableView.register(ReceiverTableViewCell.self, indexPath: indexPath)
                cell.configure(message: message[indexPath.row])
                return cell
            }
            
            
        } else {
            if isAdmin {
                let cell = tableView.register(SenderTableViewCell.self, indexPath: indexPath)
                cell.configure(message: message[indexPath.row])
                return cell
            } else {
                let cell = tableView.register(ReceiverTableViewCell.self, indexPath: indexPath)
                cell.configure(message: message[indexPath.row])
                return cell
            }
        }
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == tableView {
            
            if (scrollView.contentOffset.y < 0) {
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    Utility.showLoading()
                    self.numberOfPages += 1
                    self.offset = self.offset*self.numberOfPages
                    self.allMessages()
                }
            }
        }
    }
}

