//
//  SenderTableViewCell.swift
//  Dream-Body
//
//  Created by MAC on 11/08/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class SenderTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure (message: Message) {
        timeLabel.text = Utility.changeDateToAnyFormat(date: message.dateTime, toFormate: "HH:mm")
        messageLabel.text = message.message
    }
}
