//
//  AdminChatViewController.swift
//  Dream-Body
//
//  Created by MAC on 29/10/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit
import FirebaseDatabase
import ObjectMapper
import FirebaseFirestore



class AdminChatViewController: UIViewController {

    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    
    //MARK: - Variables
    var ref: DatabaseReference!
    var chats: [Chat] = []
    var filterArray: [Chat] = []
    var numberOfPages = 1
    var offset = 20
    var isSearching = false
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        searchTextField.setLeftPaddingPoints(10)
    }
    
    
    //MARK: - Setup
    func setup () {
        searchTextField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        ref = Database.database().reference()
        allThreads()
    }
    
    
    //MARK: - Actions
    @IBAction func logoutPressed(_ sender: Any) {
        DataManager.shared.deleteAuthentication()
        DataManager.shared.deleteUserDetail()
        Utility.loginRootViewController()
    }
    
    
    //MARK: - Private Methods
    func allThreads () {
       
        ChatManager.shared.fetchAllChats(offset: offset) { chats, error in
            
            if error == nil {
                self.chats = chats!
                self.tableView.reloadData()
            }
        }
    }
}


//MARK: - UITableViewDelegate & UITableViewDatasource
extension AdminChatViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return self.filterArray.count
        }
        return self.chats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.register(AdminChatTableViewCell.self, indexPath: indexPath)
        
        if isSearching {
            cell.configure(chat: filterArray[indexPath.row])

        } else {
            cell.configure(chat: chats[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = MessagingViewController()
        
        if isSearching {
            vc.userId = self.filterArray[indexPath.row].uid
            
        } else {
            vc.userId = self.chats[indexPath.row].uid
        }
        vc.isAdmin = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == tableView {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                self.numberOfPages += 1
                self.offset = self.offset*self.numberOfPages
                allThreads()
            }
        }
    }
}


extension AdminChatViewController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        print(textField.text ?? "")
        if textField.text == "" {
            isSearching = false
            return
        }
        isSearching = true
        filterArray.removeAll()
        filterArray = self.chats.filter({$0.name.contains( textField.text ?? "")})
        tableView.reloadData()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField.text ?? "") == "" {
            isSearching = false
            filterArray.removeAll()
            tableView.reloadData()
        }
    }
}
