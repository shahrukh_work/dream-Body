//
//  AdminChatTableViewCell.swift
//  Dream-Body
//
//  Created by MAC on 29/10/2021.
//  Copyright © 2021 codesrbit. All rights reserved.
//

import UIKit

class AdminChatTableViewCell: UITableViewCell {

    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageTextLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure (chat: Chat) {
        
        self.userImageView.sd_setImage(with: URL(string: chat.userImage),placeholderImage: #imageLiteral(resourceName: "profile_image"))
        self.messageTextLabel.text = chat.lastMessage
        self.titleLabel.text = chat.name
        timeLabel.text = Utility.changeDateToAnyFormat(date: chat.date, toFormate: "HH:mm")
    }
}
