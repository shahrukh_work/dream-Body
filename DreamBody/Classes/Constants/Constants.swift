//
//  Constants.swift
//  Doro
//
//  Created by a on 04/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

var kApplicationWindow = Utility.getAppDelegate()!.window
var skipButtonTapped = false
var retryButtonPressed = false
var uiButtonColor = false
var backButtonTapped = false
var backOnRetryTapped = false
var notificationButton = false
var tapValidate = false
var tapToCancel = false

var apiRequestObject: [String: String] = [:]

enum PaymentMethod {
    case paypal
    case venmo
    case creditCard
}


enum UserType: String {
    case donor = "1"
    case donee = "0"
}


struct APIRoutes {
    static var tokenUpdate = "/wp-json/jwt-auth/v1/update_fcm"
    static var baseUrl = "https://dreambodysystem.com"
    static var imageBaseUrl = "/api/shared/user/userImage"
    static var login = "/wp-json/jwt-auth/v1/token"
    static var verifyNumber = "/wp-json/jwt-auth/v1/verify-phone-number"
    static var userDetail = "/wp-json/jwt-auth/v1/userdetail"
    static var getDays = "/wp-json/jwt-auth/v1/chartdetail"
    static var addDay = "/wp-json/jwt-auth/v1/addDay"
    static var getUserProfile = "/api/shared/user/userProfile"
    static var weightLoss = "/wp-json/jwt-auth/v1/weightlosedetail"
    static var createUser = "/wp-json/wp/v2/users"
    static var updateUser = "/wp-json/jwt-auth/v1/update_user"
    static var addVideo = "/wp-json/jwt-auth/v1/addVideo"
    static var forgotPassword = "/api/shared/user/forgetPassword"
    static var favorite = "/wp-json/jwt-auth/v1/addtofavourite"
    static var history = "/wp-json/jwt-auth/v1/chart-history"
    static var updateValue = "/wp-json/jwt-auth/v1/updatedetail"
    static var getVideos = "/wp-json/jwt-auth/v1/getUserVideos"
}


struct K {
    static let appName = "⚡️ChatManager"
    static let senderCellNibName = "SendedMessageCell"
    static let recieverCellNibName = "RecievedMessageCell"
    static let imageSenderCellNibName = "ImageSendingCell"
    static let imageRecievingCellNibName = "ImageRecievingCell"
    static let mediaSendingCellNibName = "MediaMessageSenderCell"
    static let mediaRecievingCellNibName = "MediaMessageRecieverCell"
    static let audioMessageSenderCellNibName = "AudioMessageSenderCell"
    static let audioMessageRecieverCellNibName = "AudioMessageRecieverCell"
    static let contactsTableViewCell = "contactCell"
    static let registerSegue = "RegisterToChat"
    static let loginSegue = "LoginToChat"
    
    struct BrandColors {
        static let purple = "BrandPurple"
        static let lightPurple = "BrandLightPurple"
        static let blue = "BrandBlue"
        static let lighBlue = "BrandLightBlue"
    }
    
    struct FStore {
        struct AppUser {
            static let appUser = "AppUser"
            static let uid = "uid"
            static let firstName = "firstName"
            static let lastName = "lastName"
            static let phoneNumber = "phoneNumber"
            static let imageUrl = "imageUrl"
            static let fileType = "fileType"
            static let fileUrl = "fileUrl"
            static let vaccinated = "vaccinated"
        }
        struct Friends {
            static let friends = "Friends"
            static let chatID = "chatID"
            static let participantList = "participantList"
            static let senderID = "senderID"
            static let status = "status"
        }
        struct Chats {
            static let chats = "Chat"
            static let chatID = "chatID"
            static let isFirstMessageSend = "isFirstMessageSend"
            static let groupName = "groupName"
            static let isGroup = "isGroup"
            static let participantData = "participantData"
            static let participantList = "participantList"
            
            struct Messages {
                static let messages = "messages"
                static let messageID = "messageID"
                static let message = "message"
                static let senderID = "senderID"
                static let timeStamp = "timeStamp"
                static let fileType = "fileType"
                static let fileURL = "fileURL"
            }
        }
    }
    
    struct UserDefaults {
        static let UserId = "UserId"
        static let phoneNumber = "phoneNumber"
        static let authVerificationID = "authVerificationID"
        static let isPlaying = "isPlaying"
    }
    
    struct Media{
        static let supportedFiles = [".mov", "mp4", "png", "jpeg", "mp3", "wav"]
        static let supportedAudioFiles = ["mp3", "wav"]
        static let supportedPhotosFiles = ["png", "jpeg"]
    }
}
