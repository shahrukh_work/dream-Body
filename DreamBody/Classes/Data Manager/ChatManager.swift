//
//  ChatManager.swift
//  Flash Chat iOS13
//
//  Created by Asad Mehmood on 13/09/2021.
//  Copyright © 2021 Angela Yu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class ChatManager: NSObject {
    
    static let shared = ChatManager()
    var db: Firestore {
        get{
            return Firestore.firestore()
        }
    }
    var userID : String? {
        get {
            if let myUserID = UserDefaults.standard.string(forKey: K.UserDefaults.UserId){
                return myUserID
            } else {
                let myUserID = Auth.auth().currentUser
                return myUserID?.uid
            }
        }
    }
    
    typealias chatFetchCompletionBlock = (_ chats: [Message]?, _ error: Error?) -> Void
    typealias AllThreadsFetchCompletionBlock = (_ chats: [Chat]?, _ error: Error?) -> Void

 //   typealias appUsersFetchCompletionBlock = (_ appUsers: [AppUser]?, _ error: Error?) -> Void
 //   typealias friendsListFetchCompletionBlock = (_ friends: [Friend]?, _ error: Error?) -> Void
    typealias createNewChatCompletionBlock = (_ chat: Chat?, _ error: Error?) -> Void
  //  typealias createNewUserCompletionBlock = (_ chat: AppUser?, _ error: Error?) -> Void
    typealias doExist = (_ exist: Bool) -> Void
    typealias fetchMessagesCompletionBlock = (_ messages: [Message]?, _ error: Error?) -> Void
    typealias sendMessageCompletionBlock = (_ isSent: Bool, _ error: Error?) -> Void
   // typealias deleteMediaCompletionBlock = (_ success: Bool, _ error: Error?) -> Void
   // typealias addParticipabtToChatCompletionBlock = (_ success: Bool, _ error: Error?) -> Void

//    private override init() {
//        FirebaseApp.configure()
//    }
    
    func fetchUserChats(offset: Int, userId: String, completion: @escaping chatFetchCompletionBlock){
        var chats: [Message] = []
        ChatManager.shared.db.collection(K.FStore.Chats.chats).document(userId).collection("messages").order(by: "dateTime").limit(toLast: offset).addSnapshotListener { querySnapshot, error in
            chats = []
            if let _ = error {
                //.order(by: "recentTimeStamp")
                // this requires indexing on Firebase.
                completion(nil, error!)
            } else {
                if let documents = querySnapshot?.documents{
                    for document in documents {
                        let dictionary = document.data()
                        let chat = Message(dictionary: dictionary)
                        chats.append(chat)
                    }
                }
                completion(chats, nil)
            }
        }
    }
    
    func fetchAllChats(offset: Int, completion: @escaping AllThreadsFetchCompletionBlock){
        var chats: [Chat] = []
        
        ChatManager.shared.db.collection(K.FStore.Chats.chats)
            .order(by: "date",descending: true).limit(toLast: offset).addSnapshotListener{ querySnapshot, error in
                chats = []
                if let _ = error {
                    completion(nil, error!)
                } else {
                    if let documents = querySnapshot?.documents {
                        for document in documents {
                            let dictionary = document.data()
                            let chat = Chat(dictionary: dictionary)
                            chats.append(chat)
                        }
                    }
                    completion(chats, nil)
                }
            }
    }
    
    
    func fetchAllChats(search: String, completion: @escaping AllThreadsFetchCompletionBlock){
        var chats: [Chat] = []
        
        ChatManager.shared.db.collection(K.FStore.Chats.chats).whereField("name", isEqualTo: search)
            .order(by: "date").addSnapshotListener{ querySnapshot, error in
                chats = []
                if let _ = error {
                    completion(nil, error!)
                } else {
                    if let documents = querySnapshot?.documents {
                        for document in documents {
                            let dictionary = document.data()
                            let chat = Chat(dictionary: dictionary)
                            chats.append(chat)
                        }
                    }
                    completion(chats, nil)
                }
            }
    }
    
//    func fetchAppUsers(completion: @escaping appUsersFetchCompletionBlock){
//        ChatManager.shared.db.collection(K.FStore.AppUser.appUser).addSnapshotListener({ querySnapshot, error in
//            var appUsers: [AppUser] = []
//            if let _ = error {
//                completion(nil, error!)
//            } else {
//                if let documents = querySnapshot?.documents{
//                    for document in documents {
//                        let dictionary = document.data()
//                        let user = AppUser(dictionary: dictionary)
//                        appUsers.append(user)
//                    }
//                }
//                completion(appUsers, nil)
//            }
//        })
//    }
    
//    func fetchFriendsList(completion: @escaping friendsListFetchCompletionBlock) {
//        if let uid = userID{
//            ChatManager.shared.db.collection(K.FStore.Friends.friends)
//                .whereField(K.FStore.Friends.participantList, arrayContainsAny:[uid]).addSnapshotListener({ querySnapshot, error in
//                    var friends: [Friend] = []
//                    if let _ = error {
//                        completion(nil, error!)
//                    } else {
//                        if let documents = querySnapshot?.documents{
//                            for document in documents {
//                                let dictionary = document.data()
//                                let friend = Friend(dictionary: dictionary)
//                                friends.append(friend)
//                            }
//                        }
//                        completion(friends, nil)
//                    }
//                })
//        }
//    }
    
    func createNewChat(chatToCreate: [String: Any], completion: @escaping createNewChatCompletionBlock) {
        let newChatRef = ChatManager.shared.db.collection(K.FStore.Chats.chats).document()
        var chat = chatToCreate
        chat["chatID"] = newChatRef.documentID
        let chatModel = Chat(dictionary: chat)
        newChatRef.setData(chat) { error in
            if let error = error{
                completion(nil, error)
            } else{
                completion(chatModel, nil)
            }
        }
    }
    func fetchMessages(offset: Int, chatID: String, completion: @escaping fetchMessagesCompletionBlock) {
        db.collection(K.FStore.Chats.chats).document(chatID).collection(K.FStore.Chats.Messages.messages)
            .order(by: K.FStore.Chats.Messages.timeStamp).limit(to: offset)
            .addSnapshotListener { querySnapshot, error in
                var messages: [Message] = []
                if let _ = error{
                    completion(nil, error)
                } else {
                    if let documents = querySnapshot?.documents{
                        for document in documents {
                            let dictionary = document.data()
                            let message = Message(dictionary: dictionary)
                            messages.append(message)
                        }
                    }
                    completion(messages, nil)
                }
            }
    }
    
    
    //TODO: - Umcomment for semnding message
    func sendMessage(userId: String, type: String = "admin",withMessage composedMessage: String, completion: @escaping sendMessageCompletionBlock){

        var id = userId
        if type == "admin" {
            id = type
        }
        ChatManager.shared.db.collection(K.FStore.Chats.chats).document(userId).collection(K.FStore.Chats.Messages.messages).document().setData(["uid":id ,"message":composedMessage, "dateTime":Date()]) { error in
            if let _ = error {
                completion(false, error)
            } else {
                completion(true, nil)
            }
        }
        
    }
    
    func sendUpdateLatestByAdmin(userId: String, withMessage composedMessage: String, completion: @escaping sendMessageCompletionBlock){

        ChatManager.shared.db.collection(K.FStore.Chats.chats).document(userId).updateData(["date":Date(), "latestMessage":composedMessage]) { error in
            if let _ = error {
                completion(false, error)
            } else {
                completion(true, nil)
            }
        }
        
    }
    
    func sendUpdateLatestByUser(name: String, userId: String, withMessage composedMessage: String, imageUrl: String,completion: @escaping sendMessageCompletionBlock){

        ChatManager.shared.db.collection(K.FStore.Chats.chats).document(userId).setData(["uid": userId, "name":name,"date":Date(),"latestMessage": composedMessage, "userImage": imageUrl]) { error in
            if let _ = error {
                completion(false, error)
            } else {
                completion(true, nil)
            }
        }
        
    }
    
//    func updateFirstMessageSent(chatID: String, completion: @escaping sendMessageCompletionBlock) {
//        self.db.collection(K.FStore.Chats.chats).document(chatID).updateData([
//            "firstMessageSend": true
//        ]) { err in
//            if let _ = err {
//                completion(false, err)
//            } else {
//                completion(true, nil)
//            }
//        }
//    }
//
//    func removeMediaFromStorageAt(path: String?, completion: @escaping deleteMediaCompletionBlock){
//        if let _ = path {
//            let storage = Storage.storage()
//            var storageRef = storage.reference()
//            storageRef = storageRef.child(path!)
//
//            storageRef.delete { error in
//                if let _ = error {
//                    completion(false, error!)
//                } else {
//                    completion(true, nil)
//                }
//            }
//        }
//    }
    
//    func createNewUser(userDict: [String: Any], completion: @escaping createNewUserCompletionBlock) {
//        let userModel = AppUser(dictionary: userDict)
//        ChatManager.shared.db.collection(K.FStore.AppUser.appUser).document(userDict["uid"] as! String).setData(userDict){ error in
//            if let error = error{
//                completion(nil, error)
//            } else{
//                completion(userModel, nil)
//            }
//        }
//    }
    
//    func doCurrentUserRegistered(completion: @escaping doExist){
//        if let uid = userID{
//            ChatManager.shared.db.collection(K.FStore.AppUser.appUser)
//                .whereField(K.FStore.AppUser.uid, isEqualTo:uid).getDocuments { querySnapshot, error in
//                    if let _ = error {
//                        completion(false)
//                    } else {
//                        if let documents = querySnapshot?.documents{
//                            if documents.count == 0{
//                                completion(false)
//                            } else{
//                                completion(true)
//                            }
//                        } else {
//                            completion(false)
//                        }
//                    }
//                }
//        }
//    }
    
//    func addParticipantsInChat(chatID: String, userID: [String], completion: @escaping addParticipabtToChatCompletionBlock){
//        ChatManager.shared.db.collection(K.FStore.Chats.chats).document(chatID)
//            .updateData([
//                "participantList" : FieldValue.arrayUnion(userID)
//            ]) { error in
//                if let _ = error {
//                    completion(false, error!)
//                } else {
//                    completion(true, nil)
//                }
//            }
//    }
//    func getMessageTime(_ message: Message) -> String? {
//        if let timeStamp = Double(message.timeStamp){
//            let myTimeInterval = TimeInterval(timeStamp)
//            let date = Date(timeIntervalSince1970: TimeInterval(myTimeInterval))
//
//            let yearsAgo = date.yearsAgo
//            let monthsAgo = date.monthsAgo
//            let weeksAgo = date.weeksAgo
//            let daysAgo = date.daysAgo
//
//            let formatter = DateFormatter()
//            formatter.timeZone = TimeZone(identifier: "grigorian")
//            formatter.dateFormat = "HH:mm a EEEE, MMMM dd, yyyy"
//            formatter.amSymbol = "AM"
//            formatter.pmSymbol = "PM"
//            if yearsAgo == 0 {
//                if monthsAgo == 0 {
//                    if weeksAgo == 0 {
//                        if daysAgo == 0 {
//                            formatter.dateFormat = "h:mm a"
//                        } else {
//                            formatter.dateFormat = "EEEE h:mm a"
//                        }
//                    } else {
//                        formatter.dateFormat = "EEE, dd h:mm a"
//                    }
//                } else {
//                    formatter.dateFormat = "EEE, MMM dd h:mm a"
//                }
//            } else {
//                formatter.dateFormat = "MMM dd, yy h:mm a"
//            }
//            return formatter.string(from: date)
//        }
//        return nil
//    }
}
