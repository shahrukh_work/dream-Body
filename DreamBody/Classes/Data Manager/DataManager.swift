//
//  DataManager.swift
//  Doro
//
//  Created by a on 27/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import Foundation
import ObjectMapper

class DataManager {
    
    static let shared = DataManager()
    
    func setStringData (value: String, key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func setIntData (value: Int, key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func setFinger (enable: Bool) {
        UserDefaults.standard.set(enable, forKey: "finger")
    }
    
    func getFinger () -> Bool {
        var enabled = false

        if UserDefaults.standard.object(forKey: "finger") != nil {
            enabled = UserDefaults.standard.bool(forKey: "finger")
        }
        return enabled
    }
    
    func setFirstTimSignUp (enable: Bool) {
        UserDefaults.standard.set(enable, forKey: "first_time")
    }
    
    func getFirstTimSignUp () -> Bool {
        var enabled = false

        if UserDefaults.standard.object(forKey: "first_time") != nil {
            enabled = UserDefaults.standard.bool(forKey: "first_time")
        }
        return enabled
    }
    
    func deleteFinger () {
         UserDefaults.standard.set(nil, forKey: "finger")
    }
    
    func setAuthentication (user: String) {
        UserDefaults.standard.set(user, forKey: "auth_data")
    }
    
    func getAuthentication() -> Login? {
        var user: Login?

        if UserDefaults.standard.object(forKey: "auth_data") != nil {
            user = Mapper<Login>().map(JSONString:UserDefaults.standard.string(forKey: "auth_data")!)
        }
        return user
    }
    
    func setUser (user: String) {
        UserDefaults.standard.set(user, forKey: "user_data")
    }
    
    func getUser() -> User? {
        var user: User?

        if UserDefaults.standard.object(forKey: "user_data") != nil {
            user = Mapper<User>().map(JSONString:UserDefaults.standard.string(forKey: "user_data")!)
        }
        return user
    }
    
    func setUserDetail (user: String) {
        UserDefaults.standard.set(user, forKey: "user_detail")
    }
    
    func getUserDetail() -> UserDetail? {
        var user: UserDetail?

        if UserDefaults.standard.object(forKey: "user_detail") != nil {
            user = Mapper<UserDetail>().map(JSONString:UserDefaults.standard.string(forKey: "user_detail")!)
        }
        return user
    }
    
    func deleteUserDetail () {
         UserDefaults.standard.set(nil, forKey: "user_detail")
    }
    
    func deleteUser () {
         UserDefaults.standard.set(nil, forKey: "user_data")
    }
    
    func deleteAuthentication () {
        UserDefaults.standard.set(nil, forKey: "auth_data")
    }
    
    
    
    func setFavoriteId (id: Int) {
        var newItem: [Int] = []
        var filterData: [Int] = []
        //getFavoriteIds()
        if getFavoriteIds() != nil && getFavoriteIds()!.count != 0 {
            newItem = getFavoriteIds()!
            filterData = filterData.filter({$0 == id})
        }
        
        if filterData.count == 0 {
            newItem.append(id)
        }
        UserDefaults.standard.set(newItem, forKey: "fav_data")
    }
    
    func getFavoriteIds() -> [Int]? {
        var items: [Int]?
        
        if UserDefaults.standard.array(forKey: "fav_data") != nil {
            items = UserDefaults.standard.array(forKey: "fav_data") as? [Int]
        }
        return items
    }
    
    func removeFavoriteId(id: Int) {
        let newItem = getFavoriteIds()
        let items = newItem?.filter({$0 != id})
        //items?.removeFirst()
        UserDefaults.standard.set(items, forKey: "fav_data")
    }
}
